# Lunatics - Projet non terminé

## TODO

### Priorité

- Revoir le Mhund
  - Essayer de faire en sorte que le Targeter se charge de modifier la cible du Mhund si la cible est bloquée par un bâtiment
- Weapon 
  - Revoir le code du balai si trop incohérent 
  - Modifier le fonctionnement de l'animation
- DreamSection à commenter et modifier
- Shared à commenter et modifier
- Menu d'options
  - Système de rebind des touches
  - Options de fullscreen/fenêtré

### Dans le futur...

- Implémenter les autres armes du personnage
- La librairie
- Le Shepherd

### Bugs (connus)

- Les Moonsters ne détectent pas toujours les batîments devant eux, ceci se produit quand le Rigidbody dort
- Le Mhund peut se retrouver écrasé alors qu'il est seulement en train de frotter le mur d'un bâtiment, rendre les bâtiments statiques une fois au sol ?
- Le personnage ne s'anime pas immédiatement dans le monde de rêve