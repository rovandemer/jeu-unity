using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.Universal;

using TowerDefense.PlayerLogic;
using TowerDefense.DayNight;

/**
*   Classe à attacher sur un objet permettant de passer le jour, à attacher sur un lit.
*/
namespace TowerDefense.Functionnal {
    public class PassDay : MonoBehaviour {

        [Header("Transition parameters")]

        [SerializeField, Range(0, 10)]
        private float TransitionDuration;
        [SerializeField, Range(0, 1)]
        private float Choppiness;

        bool startedCinematic = false;

        [SerializeField]
        private Light2D[] SceneLights;

        [Header("Required for transition")]
        [SerializeField]
        private Player player;

        private SpriteRenderer bedRenderer;
        private Animator sleepAnimation;
        private bool allowSleep = false;

        private float lightIntensityStep;

        void OnValidate() {
            if (TransitionDuration < Choppiness) {
                Debug.LogError($"Choppiness should always be smaller than Transition duration");
            }
        }

        void Awake() {
            bedRenderer = GetComponent<SpriteRenderer>();
            sleepAnimation = GetComponent<Animator>();

            Debug.Assert(
                SceneLights.Length > 0,
                "Aucune lumière spécifiée"
            );
        }

        void Start() {
            DayNightManager.instance.DayStarted += () =>  { this.allowSleep = true; };

            lightIntensityStep = MaxLightIntensity(SceneLights)/TransitionDuration;
        }

        void OnMouseEnter() {
            if (allowSleep) {
                bedRenderer.color = Color.cyan;
            }
            else if (!startedCinematic) {
                bedRenderer.color = Color.red;
            }
        }

        void OnMouseExit() {
            bedRenderer.color = Color.white;
        }

        /**
        *   Quand on clique sur le lit, la cinématique est lancée.
        */
        void OnMouseUpAsButton() {        
            
            // S'il fait nuit ou si une cinématique est déjà lancée, on ne peut pas dormir.
            if (allowSleep && !startedCinematic) {
                startedCinematic = true;
                // On enregistre les bâtiments
                StartCoroutine(Cinematic());
            }
        }

        /**
        *   Coroutine lançant la cinématique de sieste et réalise la transition vers le monde de rêve
        */
        private IEnumerator Cinematic() {

            AnimateSleep();

            // On charge la scène en fond
            AsyncOperation loadScene = SceneManager.LoadSceneAsync("DreamSection");
            loadScene.allowSceneActivation = false;

            float timePassed = 0f;

            // Tant que la cinématique n'est pas terminée
            while (timePassed < TransitionDuration) {
            
                // On décrémente les lumières
                foreach (Light2D light in SceneLights) {
                    light.intensity -= lightIntensityStep*Choppiness;
                }
                
                yield return new WaitForSeconds(Choppiness);
                timePassed += Choppiness;
            }

            // On charge la scène
            loadScene.allowSceneActivation = true;
            
            // Et on reset l'animateur (le reste sera reset en rechargeant la scène)
            StopAnimatingSleep();
        }


        private float MaxLightIntensity(IList<Light2D> lights) {

            // On suppose que lights a toujours au moins une valeur
            float maxIntensity = lights[0].intensity;

            foreach (Light2D light in lights) {
                if (light.intensity > maxIntensity) {
                    maxIntensity = light.intensity;
                }
            }

            return maxIntensity;
        }

        /**
        *   Lance l'animation de sieste du joueur.
        *   Ceci est fait en desactivant le joueur, en plaçant sa caméra sur le lit et d'activer l'animateur avec la 1ère frame de sieste
        */

        private void AnimateSleep() {

            Shared.Input.GameInput.inputActions.Disable();
            
            player.transform.position = this.transform.position;
            player.gameObject.SetActive(false);

            sleepAnimation.SetBool("IsSleeping", true);

        }

        /**
        *   Fonction réciproque à AnimateSleep
        *   Replace et réactive le joueur sur son ancienne position avant de dormir et désactive l'animation de sieste
        */
        void StopAnimatingSleep() {

            Shared.Input.GameInput.inputActions.Enable();

            player.gameObject.SetActive(false);

            sleepAnimation.SetBool("IsSleeping", false);

        }

    }
}
