using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TowerDefense.Functionnal {
    public class Door : MonoBehaviour {

        [SerializeField]
        private Transform player;
        [SerializeField]
        Door teleportTo;
        [SerializeField]
        GameObject Indicator;

        [SerializeField]
        Camera.CameraPosition cameraPosition;
        [SerializeField, Range(0, 1)]
        float cameraScaleAfterEntering;

        PlayerInputActions input;

        void OnEnable() {
            input = Shared.Input.GameInput.inputActions;
            input.UI.Interact.Enable();
        }

        void OnDisable() {
            input.UI.Interact.Disable();
            input = null;
        }

        void Start() {
            Indicator.SetActive(false);
        }

        // collider ne peut être que le joueur, le layer de cet objet étant PlayerInteraction
        void OnTriggerEnter2D() {

            Indicator.SetActive(true);
            input.UI.Interact.performed += GetInRoom;
        }

        void OnTriggerExit2D() {
            Indicator.SetActive(false);
            input.UI.Interact.performed -= GetInRoom;
        }

        /**
        *  Méthode faisant appel à GetInRoomCoroutine. Elle est appelée par une interaction du joueur avec la porte
        */
        void GetInRoom(InputAction.CallbackContext _) {
            StartCoroutine(GetInRoomCoroutine());
        }

        /**
        *  Déplace le joueur dans une nouvelle salle.
        *  La taille de la caméra après avoir passé la porte est définie par cameraScaleAfterEntering.
        */
        IEnumerator GetInRoomCoroutine() {

            input.UI.Interact.performed -= GetInRoom;
            // On attend un peu avant de rentrer, plus naturel
            yield return new WaitForSeconds(0.2f);

            cameraPosition.ChangeSize(cameraScaleAfterEntering);
            cameraPosition.ChangeCameraHeight(teleportTo.transform.position.y);

            // Téléportation du joueur
            player.position = teleportTo.transform.position;
        }
    }
}
