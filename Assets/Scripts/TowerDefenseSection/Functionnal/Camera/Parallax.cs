using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 *  Effet de paralaxe appliqué sur plusieurs couches.
 *  L'ancre de la parallaxe est le Transform Anchor qui peut être le joueur ou la caméra
 */

namespace TowerDefense.Functionnal.Camera {
    public class Parallax : MonoBehaviour {

        [SerializeField]
        private Transform Anchor;

        [SerializeField, Range(0, 1)]
        private float ParallaxFactor; 

        private float individualParallax;
        private Vector3 newPosition;

        private Transform fixedBackground;

        // Ce qui sera influencé par la parallaxe
        [SerializeField]
        private SpriteRenderer[] BackgroundLayersOrderedByFurthest;
        private Transform[] backgroundTransforms;

        // Start is called before the first frame update
        void Start() {

            // Si ce script est utilisé, on suppose qu'il faut au moins un objet sur lequel appliquer la parallaxe.
            Debug.Assert(
                BackgroundLayersOrderedByFurthest.Length > 0,
                $"No {nameof(BackgroundLayersOrderedByFurthest)} attached to {this.gameObject.name}",
                this
            );

            SpriteRenderer fixedRenderer = GetComponent<SpriteRenderer>();

            if (fixedRenderer != null) {
                fixedBackground = fixedRenderer.transform;
            }
            backgroundTransforms = tabOutOfSpriteRenderers(BackgroundLayersOrderedByFurthest);

        }

        // Update is called once per frame
        void Update() {

            individualParallax = ParallaxFactor;

            // Le fond est placé sur la position de la caméra
            if (fixedBackground != null) {
                placeOnAnchor(fixedBackground);
            }
            foreach(Transform backgroundT in backgroundTransforms) {

                applyParallax(backgroundT, individualParallax);
                // Ceci correspond à l'effet de parallaxe qui fait qu'un objet proche bouge moinplus
                // quand le personnage se déplace qu'un objet lointain
                individualParallax *= 0.9f;
            }
        }

        /**
        *   Convertit un tableau de SpriteRenderer en un tableau de leurs composants transform.
        *   @param Le tableau à traduire
        *   @return Le tableau des Transforms de chaque élément du tableau en entrée
        */
        private Transform[] tabOutOfSpriteRenderers(SpriteRenderer[] spriteRenderers) {

            Transform[] transformTab = new Transform[spriteRenderers.Length];

            for (int i = 0; i < spriteRenderers.Length; i++) {
                transformTab[i] = spriteRenderers[i].transform;
            }

            return transformTab;
        }

        /**
        *   Place le transform passé en argument sur la caméra
        *   @param t Le Transform dont on change la position pour le placer sur la caméra
        */
        private void placeOnAnchor(Transform t) {
            
            // Placer sur la caméra avec un facteur de parallaxe de 1 revient à placer exactement sur la caméra
            applyParallax(t, 1f);
        }

        /**
        *   Applique un effet de parallaxe sur l'objet t
        *   @param t Le transform avec la position sur lequel appliquer la parralaxe
        *   @param parallaxFactor Le facteur de la parallaxe (s'il vaut 0, l'objet ne bouge pas, s'il vaut 1 l'objet suit parfaitement la caméra).
        */
        private void applyParallax(Transform t, float parallaxFactor) {
            
            newPosition = t.position;

            // La seule valeur qui change est la valeur d'abscisse
            // Un facteur de parallaxe plus élevé indique que l'objet doit mieux suivre la caméra 
            // (C'est le cas d'un objet plus éloigné)
            newPosition.x = Anchor.position.x * parallaxFactor;
            t.position = newPosition;
            
        }
    }
}
