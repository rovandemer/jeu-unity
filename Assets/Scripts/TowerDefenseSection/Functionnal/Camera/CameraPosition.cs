using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * @brief Script figeant la caméra sur sa hauteur initiale
 */
namespace TowerDefense.Functionnal.Camera {
    public class CameraPosition : MonoBehaviour {

    [SerializeField]
    UnityEngine.Camera cam;
    float initialSize;
    Vector3 camPosition;

    [SerializeField]
    private Transform player;

    [Header("Borders")]

    [SerializeField]
    private Transform leftBorder;
    [SerializeField]
    private Transform rightBorder;

    private float maxLeftBorder;
    private float maxRightBorder;


    void Awake() {
        cam.orthographic = true;
        initialSize = cam.orthographicSize;
        camPosition = cam.transform.position;
    }


    void Update() {

        // Les bords sont définis dans Update afin de prendre en compte d'éventuelles modifications du ratio de l'écran
        DefineBorders();
        // La position horizontale est celle du joueur coincée entre la bordure gauche et droite.
        camPosition.x = Mathf.Clamp(player.position.x, maxLeftBorder, maxRightBorder);
        // Position verticale (sauf appel de ChangeCameraHeight) et profondeur ne bougent pas
        cam.transform.position = camPosition;
        
    }

    /**
     *  Change la hauteur de la caméra.
     */
    public void ChangeCameraHeight(float newHeight) {
        camPosition.y = newHeight;
        cam.transform.position = camPosition;
    }

    /**
     *  Modifie la taille de la caméra
     *  @param newSize 1 remet la valeur par défaut <1 réduit la taille et >1 augmente la taille de la caméra
     */
    public void ChangeSize(float newSize) {
        cam.orthographicSize = initialSize*newSize;
    }

    /**
     *  Définit les bords que la caméra ne peut pas dépasser.
     */
    void DefineBorders() {

        float borderOffsetToCenter = GetCamXOffset();
        // Mur à gauche, donc négatif, le bord gauche de la caméra étant à -camXOffsetBorder, 
        // On ajoute camOffsetBorder comme valeur maximale que peut atteindre horizontalement la caméra
        maxLeftBorder = leftBorder.position.x + borderOffsetToCenter;
        // Mur à droite, le bord droit est à camXOffsetBorder cette fois
        maxRightBorder = rightBorder.position.x - borderOffsetToCenter;
        // Les deux objets de bords ne servent plus


    }

    /**
     *  Récupère la distance horizontale entre le centre de la caméra et ses bords horizontaux.
     */
    float GetCamXOffset() {
        // Position horizontale droite - centre
        return cam.ViewportToWorldPoint(Vector3.right).x - cam.transform.position.x;
    }
    }
}
