using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using TowerDefense.ObjectiveNS;
using TowerDefense.PlayerLogic;

namespace TowerDefense.Functionnal {
    public class GameOver : MonoBehaviour {

        [SerializeField]
        private Player player;
        [SerializeField]
        private Objective objective;

        [SerializeField]
        private Shared.Indicators.KeepLoaded destroyOnGameOver;

        void Start() {
            objective.HasDied += GameOverScreen;
            player.HasDied += GameOverScreen;
        }


        void GameOverScreen(Damageable deadPointOfImportance) {
            destroyOnGameOver.Destroy();

            StartCoroutine(LoadGameOverScene(deadPointOfImportance));            
        }

        IEnumerator LoadGameOverScene(Damageable deadPointOfImportance) {

            Time.timeScale = 0f;

            AsyncOperation sceneLoad = SceneManager.LoadSceneAsync("GameOverMenu");
            sceneLoad.allowSceneActivation = false;
            Time.timeScale = 0f;
            // TODO : deadPointOfImportance pourrait avoir une animation de mort à ce moment là (qui définirait aussi la durée du gameOver)
            do {
                yield return false;
            } while (sceneLoad.progress < 0.9f);

            Time.timeScale = 1f;
            sceneLoad.allowSceneActivation = true;

        }
    }
}
