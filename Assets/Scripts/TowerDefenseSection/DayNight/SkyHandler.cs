using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;


namespace TowerDefense.DayNight {
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(Light2D))]
    public class SkyHandler : MonoBehaviour {

        // Evenements indiquant que le jour ou la nuit on commencé
        public event Action FullyNight;
        public event Action FullyDay;

        [Header("Transition properties")]

        [SerializeField]
        private Sprite DayBackground;
        [SerializeField]
        private Sprite NightBackground;

        [SerializeField]
        private float NightLightIntensity = 0.3f;
        [SerializeField]
        private float DayLightIntensity = 0.9f;

        [Header("Transition parameters")]

        [SerializeField]
        private float _transitionDuration;
        public float TransitionDuration {
            get => _transitionDuration;
        }
        [SerializeField]
        private float AlphaTransitionSpeed = 1f;

        // Le temps en seconde entre chaque décrément/incrément de l'intensité lumineuse du fond.
        // Une valeur de 0 crée un fondu lisse, une valeur plus grande des décréments plus grand et moins fréquents
        [SerializeField]
        private float Choppiness;

        private SpriteRenderer background;
        private Light2D backgroundLight;
        private float lightIntensityStep;

        void OnValidate() {
            if (TransitionDuration < Choppiness) {
                Debug.LogError($"Choppiness should always be smaller than Transition duration");
            }
        }

        void Awake() {

            this.background = GetComponent<SpriteRenderer>();
            this.backgroundLight = GetComponent<Light2D>();

            this.lightIntensityStep = (DayLightIntensity - NightLightIntensity)/TransitionDuration;

        }

        void Start() {
            DayNightManager.instance.StartDay += () => { this.StartCoroutine( Day() ); };
            DayNightManager.instance.StartNight += _ => { this.StartCoroutine( Night() ); };
        }

        /**
        *   Commence la nuit. Réciproque à StartDay.
        *   Change le fond vers le fond de nuit, réactive la Lune et diminue l'intensité du fond.
        *   Si la luminosité est déjà inférieure ou égale à celle de jour, seul le sprite est changé sans aucun changement de luminosité visible.
        */
        private IEnumerator Night() {

            float currentVelocity = 0f;

            // On diminue l'intensité lumineuse à chaque passage jusqu'à atteindre NightLightIntensity
            while (backgroundLight.intensity > NightLightIntensity) {
                
                // LightIntensityStep a été calculé dans le Awake de telle sorte à ce que l'on atteigne la valeur NightLightIntensity
                // en TransitionDuration secondes, sauf que l'on attend "Choppiness" secondes entre chaque passage, donc on multiplie par Choppiness 
                // pour conserver TransitionDuration comme temps entre les transitions
                // (Choppiness correspond donc ici au temps d'attente et la force entre chaque décrément d'intensité lumineuse).
                backgroundLight.intensity -= lightIntensityStep * Choppiness;
                // On diminue également la valeur alpha de la couleur du fond
                AlphaDamp(background, NightLightIntensity, ref currentVelocity, AlphaTransitionSpeed);
                
                yield return new WaitForSeconds(Choppiness);
            }

            // On rend à nouveau le sprite du ciel opaque
            background.color = new Color(background.color.r, background.color.g, background.color.b, 1f);
            background.sprite = NightBackground;

            FullyNight?.Invoke();
        }

        /**
        *  Coroutine passant de la nuit au jour.
        *  Change le fond (opacité et sprite) et l'intensité lumineuse
        *  Si la luminosité est déjà supérieure ou égale à celle de jour, seul le sprite est changé sans aucun changement de luminosité visible.
        */
        private IEnumerator Day() {

            // On rend le sprite du ciel non-opaque
            background.color = new Color(background.color.r, background.color.g, background.color.b, 0.2f);
            background.sprite = DayBackground;

            float currentVelocity = 0f;

            // Monte l'intensité lumineuse jusqu'à atteindre DayLightIntensity
            while (backgroundLight.intensity < DayLightIntensity) {

                // LightIntensityStep a été calculé dans le Awake de telle sorte à ce que l'on atteigne la valeur DayLightIntensity depuis NightLightIntensity
                // en TransitionDuration secondes, sauf que l'on attend "Choppiness" secondes entre chaque passage, donc on multiplie par Choppiness 
                // pour conserver TransitionDuration comme temps entre les transitions
                // (Choppiness correspond donc ici au temps d'attente et la force entre chaque incrément d'intensité lumineuse).
                backgroundLight.intensity += lightIntensityStep * Choppiness;
                AlphaDamp(background, DayLightIntensity, ref currentVelocity, AlphaTransitionSpeed);

                yield return new WaitForSeconds(Choppiness);
            }

            background.color = new Color(background.color.r, background.color.g, background.color.b, 1f);
            FullyDay?.Invoke();
        }

        
        /**
        *  Damp de la couleur du background.
        */
        private static void AlphaDamp(SpriteRenderer background, float target, ref float currentVelocity, float waitingTime) {
            // On diminue le alpha graduellement pour un fondu plus lisse
            Color color = background.color;
            color.a = Mathf.SmoothDamp(color.a, target, ref currentVelocity, 0.02f*waitingTime);
            background.color = color;
        }

    }
}
