using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
*   Script déplacant la Lune de manière paramétrable autour d'un cercle
*/

namespace TowerDefense.DayNight {
    public class MoonMovement : MonoBehaviour {

        [SerializeField]
        private SkyHandler skyHandler;
        [SerializeField]
        private TowerDefense.Moonsters.Management.WaveSpawner waveSpawner;

        // Paramètre qui sera incrémenté
        private float t;

        [Header("Rotation parameters")]

        // Relatifs au parent
        [SerializeField]
        private float xOffset = 0f;
        [SerializeField]
        private float yOffset = -1f;

        [SerializeField, Range(0, 10)]
        private float radius = 2f;
        [SerializeField, Range(0, 1)]
        private float movementSpeed = 0.01f;
        private float currentSpeed;

        public event Action FinishedRotation;

        void Awake() {
            // La lune commence légèrement dans la partie inférieure de son rayon (pour pas qu'elle apparaisse de nulle part).
            t = -Mathf.PI/8;

            // Quand la lune a fini de tourner, on la désactive
            FinishedRotation += () => {
                this.gameObject.SetActive(false);
            };

            // Quand il fera pleinement nuit, on lancera le mouvement de la Lune
            DayNightManager.instance.NightStarted += _ => {
                this.StartCoroutine(MoveMoon()); 
            };

        }

        IEnumerator MoveMoon() {  

            currentSpeed = movementSpeed;

            while (this.t < 5*Mathf.PI/4) {

                ModifyMoonSpeedIfNeeded();

                this.t += currentSpeed*Time.deltaTime;
                this.transform.localPosition = this.MoonPosition();

                yield return false;
            }

            FinishedRotation?.Invoke();
        }

        void ModifyMoonSpeedIfNeeded() {

            bool areMoonstersPresent = waveSpawner.AreMoonstersPresent();
        
            // S'il y a encore des monstres au milieu de la nuit, on ralentit
            if (this.t > Mathf.PI/2 && areMoonstersPresent) {
                currentSpeed = 0.1f*movementSpeed;
            }
            // Sinon, s'il n'y a plus de monstres, on accélère
            else if (!areMoonstersPresent) {
                currentSpeed = 5f*movementSpeed;
            }
        }

        /**
        *   Renvoie un Vector2 qui indique à la lune à chaque instant t où elle doit se trouver.
        *   Utilise une équation paramétrique de cercle.
        */
        Vector2 MoonPosition() {

            // La lune bouge par une équation de cercle centrée en (xOffset, yOffset)
            // La lune part de la gauche vers la droite, sens inverse de l'abscisse du cercle trigo, d'où le signe moins devant le cosinus
            // 2 et 3 sont arbitaires, ils ont pour objectif de rendre plus elliptique la rotation
            return new Vector2(
                xOffset - 3*radius*Mathf.Cos(t),
                yOffset + 2*radius*Mathf.Sin(t)
            );

        }
    }
}
