using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.DayNight {
    public class ParticleManager : MonoBehaviour {
        
        [SerializeField]
        private ParticleSystem[] NightParticles;
        [SerializeField]
        private ParticleSystem[] DayParticles;

        void Start() {
            DayNightManager.instance.NightStarted += Night;
            DayNightManager.instance.DayStarted += Day;
        }

        /**
        *   Active les particules de jour, appelable par d'autres classes
        */
        private void Day() {
            ActivateParticles(DayParticles);
            DeactivateParticles(NightParticles);
        }

        private void Night(int nightNumber) {
            Night();
        }

        /**
        *   Active les particules de nuit, appelable par d'autres classes
        */
        private void Night() {
            ActivateParticles(NightParticles);
            DeactivateParticles(DayParticles);
        }

        /**
        *   Rend visible les ParticleSystem données en arguments en les jouant.
        *   @param particles ParticleSystem auquels appliquer la méthode Play
        */
        private void ActivateParticles(ParticleSystem[] particles) {
            foreach (ParticleSystem particle in particles) {
                particle.gameObject.SetActive(true);
            }
        }

        /**
        *   Rend invisible les ParticleSystem données en arguments en les arrêtant.
        *   @param particles ParticleSystem auquels appliquer la méthode Stop
        */
        private void DeactivateParticles(ParticleSystem[] particles) {
            foreach (ParticleSystem particle in particles) {
                particle.gameObject.SetActive(false);
            }
        }
    }
}
