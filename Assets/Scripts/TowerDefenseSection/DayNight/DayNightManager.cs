using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using Shared.Indicators;

/**
 *  Classe contenant les évenements qui démarrent le jour et la nuit
 *  Pour cela, elle se base en grande partie sur la lune, référencée par MoonMovement
 */
namespace TowerDefense.DayNight {
    public class DayNightManager : MonoBehaviour {

        public static DayNightManager instance {get; private set;} = null;

        public event Action StartDay;
        // Renvoie le numéro de la nuit précédant la nuit qui va arriver
        public event Action<int> StartNight;

        public event Action DayStarted;
        // Renvoie le numéro de nuit lorsque l'évenement est lancé
        public event Action<int> NightStarted;

        [SerializeField]
        private MoonMovement Moon;
        [SerializeField]
        private SkyHandler sky;

        private static int _nightNumber = 0;
        public static int NightNumber {
            get => _nightNumber;
            private set {
                _nightNumber = value;
                NightCounter.instance.UpdateNightCount(value);
            }
        }
        // Cet attribut décide si l'on commence par une nuit ou une journée
        private bool StartWithNight = true;

        /**
        *  Les valeurs par défaut peuvent être modifiées par des données de sauvegarde
        */
        public void LoadSaveData(Shared.Save.Data.PlayerData data) {
            if (data == null) {
                NightNumber = 0;
                StartWithNight = true;
            }
            else {
                NightNumber = data.nightNumber;
                StartWithNight = data.isNight;
            }
        }

        void Awake() {
            // Rend la création de la classe unique et accessible depuis tous les scripts (DayNightManager.instance)
            if (instance != null) {
                Debug.LogError($"Second instance of {instance.GetType()} invalid.");
                Debug.LogError($"Please delete the second {instance.GetType()} from {instance.gameObject.name} or from {this.gameObject.name}");
                return;
            }
            instance = this;

            DefineMoonEvents();
            ListenToSkyEvents();
        }

        /**
        *  Quand la nuit commence, on active la lune
        *  Quand la lune a fini sa rotation, on commence le jour
        */
        private void DefineMoonEvents() {
            StartNight += _ => {
                Moon.gameObject.SetActive(true);
            };

            Moon.FinishedRotation += () => {
                StartDay?.Invoke();
            };
        }

        /**
        *  Le SkyHandler nous notifie quand le ciel est passé effectivement à la nuit/ au jour.
        *  Afin de centraliser la propagation d'évenements à cette classe tout en laissant la responsabilité à SkyHandler, on propage seulement l'évenement à nos évenements NightStarted et DayStarted
        */
        private void ListenToSkyEvents() {
            sky.FullyNight += OnNightFell;

            sky.FullyDay += () => {
                DayStarted?.Invoke();
            };
        }

        IEnumerator Start() {

            // On attend la frame d'après avant de lancer Start.
            // Ceci permet aux autres MonoBehaviours de s'abonner à l'évenement NightStarted avant qu'il soit lancé la première fois
            // Ceci permet également à SaveHandler de modifier la variable StartWithNight ou NightNumber 
            // si les données de sauvegarde indiquent qu'il faisait jour avant que le joueur quitte sa partie.
            yield return false;

            if (StartWithNight) {
                // On commence par une nuit
                StartNight?.Invoke(NightNumber);
            }
            else {
                StartDay?.Invoke();
            }
        }

        private void OnNightFell() {
            ++NightNumber;
            NightStarted?.Invoke(NightNumber);
        }
    }
}
