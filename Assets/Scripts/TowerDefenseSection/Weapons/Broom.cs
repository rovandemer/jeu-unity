using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Weapons {
    [RequireComponent(typeof(Weapon))]
    public class Broom : MonoBehaviour, Damager {

        // Délégation sur _AbstractWeapon
        private Weapon weapon;
        private TowerDefense.PlayerLogic.PlayerController player;
        private ContactFilter2D filterMoonsters;

        public int AttackPower {
            get => weapon.AttackPower;
        }

        public float AttackCooldown {
            get => weapon.AttackCooldown;
        }

        public float AttackRange {
            get => weapon.AttackRange;
        }

        public Vector2 Knockback {
            get => weapon.Knockback;
        }

        void Start() {

            weapon = GetComponent<Weapon>();
            player = GetComponentInParent<TowerDefense.PlayerLogic.PlayerController>();

            // Le filtre est utilisé pour déterminer ce qui est une cible de l'arme (donc, dans notre cas, un Moonster)
            filterMoonsters.layerMask = LayerMask.GetMask("Moonster");
            filterMoonsters.useLayerMask = true;

        }

        public void Attack() {
            // Délégation
            weapon.Attack();
        }

        public IEnumerator AnimateAttack() {
            // Délégation (n'est jamais appelé par Broom)
            return weapon.AnimateAttack();
        }

        public List<Damageable> HitTargets() {

            // Si jamais, on peut supposer qu'il y ait un nombre maximal de Moonster à un instant donné (par exemple une dizaine)
            // Ceci permettrait de se passer de la liste

            List<RaycastHit2D> raycasts = new List<RaycastHit2D>(5);
            // On récupère la liste des Moonsters qui sont ciblés par l'attaque
            Physics2D.Raycast(transform.position, player.IsFacingRight ? Vector3.right: Vector3.left, filterMoonsters, raycasts, distance: weapon.AttackRange);

            List<Damageable> hitTargets = new List<Damageable>(5);
            foreach (RaycastHit2D raycast in raycasts) {
                hitTargets.Add(raycast.collider.gameObject.GetComponent<Damageable>());
            }

            return hitTargets;

        }
    }
}
