using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TowerDefense.Weapons.Management {
    public class WeaponManager : MonoBehaviour {

        private PlayerInputActions input;

        [SerializeReference]
        private List<Weapon> Weapons;

        private int currentlyUsedWeaponIndex = 0;
        private Weapon currentWeapon;

        private float timeSinceLastShot = 0f;

        // Start is called before the first frame update
        void Awake() {

            currentWeapon = Weapons[currentlyUsedWeaponIndex];
            currentWeapon.gameObject.SetActive(true);

        }

        void OnEnable() {

            input = Shared.Input.GameInput.inputActions;

            input.Player.Switch.Enable();
            input.Player.Fire.Enable();

            input.Player.Switch.performed += SwitchWeapon;
            input.Player.Fire.performed += Fire;

        }

        void OnDisable() {

            input.Player.Fire.performed -= Fire;
            input.Player.Switch.performed -= SwitchWeapon;

            input.Player.Switch.Disable();
            input.Player.Fire.Disable();
            
            input = null;

        }

        void Fire(InputAction.CallbackContext _) {

            if (Time.time - timeSinceLastShot > currentWeapon.AttackCooldown) {
                currentWeapon.Attack();
                timeSinceLastShot = Time.time;
            }

        }

        void SwitchWeapon(InputAction.CallbackContext context) {

            // On récupère l'information sur si le joueur veut l'arme suivante ou précédente (-1 ou 1)
            int increment = Mathf.RoundToInt(context.ReadValue<float>());

            // Modulo positif (renvoie toujours une valeur dans [0, Weapons.Count[ )
            currentlyUsedWeaponIndex = (currentlyUsedWeaponIndex + increment + Weapons.Count) % Weapons.Count;

            // On désactive l'ancienne arme et on active la nouvelle
            currentWeapon.gameObject.SetActive(false);
            currentWeapon = Weapons[currentlyUsedWeaponIndex];
            currentWeapon.gameObject.SetActive(true);

        }

    }
}
