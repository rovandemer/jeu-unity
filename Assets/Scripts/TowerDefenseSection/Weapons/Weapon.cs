using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Weapons {

    public class Weapon : MonoBehaviour {

        // Champs utiles pour l'arme, lisible par l'arme concrète ensuite
        [SerializeField]
        private int _attackPower;

        public int AttackPower {
            get => _attackPower;
        }

        [SerializeField]
        private float _attackCooldown;

        public float AttackCooldown {
            get => _attackCooldown;
        }

        [SerializeField]
        private float _attackRange;

        public float AttackRange {
            get => _attackRange;
        }

        [SerializeField]
        private Vector2 _knockback;

        public Vector2 Knockback {
            get => _knockback;
        }

        [SerializeField]
        private float AttackAnimationTime;

        private TowerDefense.PlayerLogic.PlayerController player;
        private Damager weapon;

        private Animator weaponAnimator;
        private Animator playerAnimator;

        void Start() {

            player = GetComponentInParent<TowerDefense.PlayerLogic.PlayerController>();
            weapon = GetComponent<Damager>();
            playerAnimator = player.gameObject.GetComponent<Animator>();
            weaponAnimator = GetComponent<Animator>();

            // Afin d'éviter de lancer des coroutines inutilement
            // si jamais une arme a une animation qui dépasse son cooldown d'attaque
            if (AttackAnimationTime > AttackCooldown) {
                AttackAnimationTime = AttackCooldown;
            }

        }

        /**
        *  Effectue l'attaque en demandant à weapon de fournir la liste des Damageable à blesser
        */
        public void Attack() {

            // On anime l'attaque, même si elle ne touche personne
            StartCoroutine(AnimateAttack());

            // On demande à l'arme de nous envoyer un tableau de Damageable à blesser
            List<Damageable> hitEnemies = weapon.HitTargets();

            // Pour chaque ennemi ciblé par l'arme
            foreach (Damageable moonsterDamager in hitEnemies) {

                // L'ennemi se prend des dégâts et du recul
                moonsterDamager.TakeDamage(AttackPower);
                // Le recul est orienté dans le sens de l'attaque du joueur
                moonsterDamager.hitbox.attachedRigidbody.AddForce(player.IsFacingRight ? Knockback : -Knockback);

            }
        }



        // TODO : Trouver un moyen dans l'animateur de mettre le booléen à vrai, faire l'attaque et à la fin de l'animation remettre à faux
        // TODO : Attaque non-générique du joueur, trouver autre chose (champ paramétrable d'animation controller donnant un animateur de perso ?)
        /**
        *
        */
        public IEnumerator AnimateAttack() {

            // On active l'animation d'attaque du joueur et de son arme
            playerAnimator.SetBool("IsAttacking", true);
            weaponAnimator.SetBool("IsAttacking", true);
            // On attend le temps que l'animation se termine
            yield return new WaitForSeconds(AttackAnimationTime);
            // Puis on repasse à l'animation idle
            playerAnimator.SetBool("IsAttacking", false);
            weaponAnimator.SetBool("IsAttacking", false);

        }

    }
}
