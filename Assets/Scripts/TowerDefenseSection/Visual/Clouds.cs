using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Visual {
    [RequireComponent(typeof(SpriteRenderer))]
    public class Clouds : MonoBehaviour {

        // Vitesse à laquelle doivent se déplacer les nuages
        // (1 est déja trop rapide)
        [Range(0,1)] public float speed;

        private Transform cloudTransform;
        private Vector3 newPosition;
        private Vector3 startPosition;

        private float imageSize;

        // Start is called before the first frame update
        void Start() {
            
            SpriteRenderer clouds = GetComponent<SpriteRenderer>();
            cloudTransform = clouds.transform;

            newPosition = cloudTransform.localPosition;
            startPosition = cloudTransform.localPosition;

            imageSize = SpriteWidth(clouds.sprite);
        }

        // Update is called once per frame
        void Update() {

            // Afin que les nuages ne disparaissent pas à l'infini
            // Quand on a dépassé la taille de l'image, on peut reboucler et la replacer sur sa position initiale
            newPosition.x += speed*Time.deltaTime;
            newPosition.x = newPosition.x % imageSize;
            cloudTransform.localPosition = newPosition;

        }

        /**
        *   Fonction récupérant la largeur d'un sprite.
        *   @ return La largeur du sprite en worldUnits
        */
        static float SpriteWidth(Sprite s) {
            return (float) s.rect.width / s.pixelsPerUnit;
        }
    }
}
