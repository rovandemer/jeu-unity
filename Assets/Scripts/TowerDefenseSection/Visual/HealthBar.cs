using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Visual {
    public class HealthBar : MonoBehaviour {

        [SerializeField]
        private Image fill;
        [SerializeField]
        private Color healthBarColor;
        [SerializeField]
        private Image icon;

        private Damageable damageable;
        private Slider healthBar;

        private static Camera _camera = null;
        private Camera WorldCamera {

            get {
                if (_camera == null) {
                    _camera =  GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
                }
                return _camera;
            }

        }

        // Start is called before the first frame update
        void Awake() {

            // Initialise correctement la caméra, même si l'objet est chargé depuis un Prefab
            GetComponent<Canvas>().worldCamera = WorldCamera;
            healthBar = GetComponentInChildren<Slider>();
            fill.color = healthBarColor;
        }

        void Start() {

            damageable = GetComponentInParent<Damageable>();
            healthBar.maxValue = damageable.Health;
            icon.sprite = damageable.HealthBarIcon;
            
        }

        // Update is called once per frame
        void Update() {

            healthBar.value = (damageable.Health > 0) ? damageable.Health : 0;

            // Si le damageable a fait un demi-tour sur lui même, on fait en sorte que la barre de vie également.
            // Ainsi, elle est toujours orienté du même côté
            this.transform.localRotation = damageable.transform.localRotation;

        }
    }
}
