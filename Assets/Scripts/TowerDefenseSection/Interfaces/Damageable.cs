using UnityEngine;
using System;

/**
 * Interface permettant de savoir si une entité peut recevoir des dégats.
 */

public interface Damageable {
    /**
     *  Icone du Damageable
     */
    Sprite HealthBarIcon {get;}

    /**
     *  Récupère le nombre de PV de l'entité.
     */
    int Health {get;}

    /**
     *  Transform du Damageable
     */
    Transform transform {get; }
    GameObject gameObject {get; }

    /**
     *  Informations sur son rigidBody et son collider
     */
    Collider2D hitbox {get;}

    /** 
     * Retire à l'entité un certain nombre de points de vie
     * @param hpToRemove Le nombre de points de vie à retirer.
     */
    void TakeDamage(int hpToRemove);
    
    /**
     * Prend en charge la mort d'une entité.
     */
    void Die();

    event Action<Damageable> HasDied;

}