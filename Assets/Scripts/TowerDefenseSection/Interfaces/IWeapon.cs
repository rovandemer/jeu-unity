using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Damager {

    List<Damageable> HitTargets();

}
