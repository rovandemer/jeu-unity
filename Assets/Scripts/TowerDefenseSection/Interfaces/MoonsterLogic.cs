using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Moonsters.Targeters {
    public interface MoonsterLogic {

        /**
         *  Renvoie parmi la liste de Collider2D de Damageable le transform du Damageable cible du Moonster
         *  Ne doit jamais renvoyer null, peut éventuellement renvoyer un objet détruit
         */
        Collider2D PrioritizeTarget(List<Collider2D> potentialTargets);

        /**
         *  Renvoie la vraie cible du Moonster, en sachant qu'il a l'information que blocking l'empêche d'atteindre
         *  ce qu'a renvoyé PrioritizeTarget (valeur donnée par priorityTarget).
         */
        Collider2D HandleBlocking(Collider2D priorityTarget, Collider2D blocking);
    }
}
