using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Buildings.Components {
    public class ShootingZone : MonoBehaviour {

        CircleCollider2D attackRange;

        // Se comportera plus ou moins comme une structure de file
        // si l'on suppose que tous les ennemis vont à la même vitesse
        private List<GameObject> enemies = new List<GameObject>();
        [SerializeField, Range(0, 100f)]
        private float radius;

        private GameObject lastEnemyObject;
        private Damageable lastTarget;

        void Start() {

            this.attackRange = gameObject.GetComponent<CircleCollider2D>();
            attackRange.isTrigger = true;
            attackRange.radius = radius;
            
        }

        /**
        *   Détecte les collisions avec les ennemis.
        *   Puisque ShootingZone a comme Layer "Detect Moonster", seul les Colliders de Moonsters sont considérés.
        *   
        *   @param collider Collider2D d'une cible
        */
        private void OnTriggerEnter2D(Collider2D collider) {
            
            // Met l'ennemi qui vient d'être inséré à la liste à la fin (don)
            enemies.Add(collider.gameObject);
            
        }

        /**
        *   Supprime un Moonster qui serait sorti de la zone de tir.
        *
        *   @param collider Collider2D d'une cible
        */
        private void OnTriggerExit2D(Collider2D collider) {

            // Puisque Remove supprime le premier élément qu'il trouve en partant du début
            // Et que Add ajoute à la fin
            // L'ennemi qui est sorti à un instant donné est (si l'on suppose leurs vitesses équivalentes)
            // probablement proche du début de la liste (il est le dernier parmi ceux encore présent à avoir été rajouté.)
            enemies.Remove(collider.gameObject);

        }

        /**
        *  Renvoie le dernier monstre à avoir entré la Collision2D de ShootingZone.
        *  @remark Pour des raisons de performances, stocke dans les variables lastEnemyObject et lastTarget
        *  @remark le collider et le monstre renvoyé pour y faire appel si jamais il n'a pas changé.
        */
        private Damageable SendLastEnemy() {

            /*
            *   Pour des raisons de permormance, renvoie l'ennemi qui est en dernier entré dans la zone de tir.
            *   Si le comportement devient trop étrange (si les ennemis ont des vitesses trop variables),
            *   on peut essayer de trier la liste en fonction de la distance avec le centre et renvoyer celui-là.
            */

            // Si l'on a pas changé de cible entre deux appels de cette fonction
            if (enemies[0] == lastEnemyObject) {
                // Alors on a pas changé de Moonster
                return lastTarget;
            }

            // Sinon, il faut mettre à jour lastEnemyObject et lastTarget
            lastEnemyObject = enemies[0];
            // GetComponent peut être coûteux dans une méthode qui a pour but d'être appelée dans un Update.
            lastTarget = lastEnemyObject.GetComponent<Damageable>();

            return lastTarget;
        
        }

        /**
        *   Renvoie la cible à viser.
        */
        public Damageable Target() {

            if (enemies.Count == 0) {
                return null;
            }

            return this.SendLastEnemy();
        }




        /******************************************************/
        /*                      Inutilisé                     */
        /******************************************************/

        /**
        *   Récupère l'énemi le plus proche de la tour.
        *  @param enemies Liste des ennemis dans la zone de tir
        */
        private Collider2D GetClosestEnemy(HashSet<Collider2D> enemies) {

            // On récupère la position de la tour
            Vector3 enemyPosition;
            // On initialise la distance minimale à une valeur très grande
            float minDistance = Mathf.Infinity;
            // On initialise l'ennemi le plus proche à null
            Collider2D closestEnemy = null;

            // Pour chaque ennemi dans la zone de tir
            foreach (Collider2D enemy in enemies) {

                // On récupère la position de l'ennemi
                enemyPosition = enemy.transform.position;

                // On calcule la distance entre l'ennemi et la tour
                float distance = this.transform.position.x - enemyPosition.x;

                // Si la distance est plus petite que la distance minimale
                if (distance < minDistance) {

                    // On met à jour la distance minimale
                    minDistance = distance;
                    // On met à jour l'ennemi le plus proche
                    closestEnemy = enemy;

                }

            }

            // On renvoie l'ennemi le plus proche
            return closestEnemy;

        }

    }
}