using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Buildings {
    public class Building : MonoBehaviour, Damageable {

        [Header("Damageable attributes")]
        [SerializeField]
        private Sprite _healthBarIcon;
        public Sprite HealthBarIcon {
            get => _healthBarIcon;
        }

        [SerializeField]
        private int _initialHealth;
        private int _damageTaken = 0;
        public int Health {
            get => _initialHealth - _damageTaken;
        }

        private bool isTakingDamage = false;
        
        public event Action<Damageable> HasDied;

        public Collider2D hitbox {get; private set;}

        [Header("Building attributes")]
        [SerializeField]
        private int _attackPower;
        public int AttackPower{
            get => _attackPower;
        }

        [SerializeField]
        private float _attackCooldown;
        public float AttackCooldown {
            get => _attackCooldown;
        }

        [SerializeField]
        private int _cost;
        public int Cost {
            get => _cost;
        }

        private SpriteRenderer gameSpriteRenderer;
        private Vector3 objectivePosition;

        void Awake() {

            hitbox = GetComponent<Collider2D>();
            gameSpriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Start() {

            // Building layer (n'interagit pas avec le joueur.)
            this.gameObject.layer = LayerMask.NameToLayer("Building");
            // Tag de Building, peut être trouvé par Tag
            this.gameObject.tag = "Building";

            GameObject objective = GameObject.Find("Objective");
            if (objective == null) {
                Debug.Log("Objectif non-trouvé");
                objectivePosition = Vector3.zero;
            }
            else {
                objectivePosition = objective.transform.position;
            }

        }

        /**
        *   Crée une copie de prefab sur la position donnée. Le tourne si nécessaire.
        *   @param prefab Bâtiment à copier
        *   @param position Position où se trouvera la copie
        *   @return Référence vers le bâtiment créé
        */
        public Building Build(Vector3 position) {

            Building building = Instantiate(this, position, Quaternion.identity);
            building.gameObject.name = building.gameObject.name.Replace("(Clone)", "");
            return building;

        }

        /**
        *  Tourne le bâtiment de telle sorte à être dos à l'objectif.
        */
        public void FlipBuildingAwayFromObjective() {

            if (this.gameObject.transform.position.x > objectivePosition.x) {
                this.transform.Rotate(Vector3.up, 180f);
            }

        }

        public void Die() {
            
            HasDied?.Invoke(this);
            Destroy(this.gameObject);

        }

        IEnumerator IndicateDamage() {
            isTakingDamage = true;
            this.gameSpriteRenderer.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            this.gameSpriteRenderer.color = Color.white;
            isTakingDamage = false;
        }

        public void TakeDamage(int damage) {
            
            _damageTaken += damage;
            if (!isTakingDamage) {
                StartCoroutine(IndicateDamage());
            }
            if (Health < 0) {
                Die();
            }
        }

        /**
        *  Appelé par le BuildingManager lorsqu'il a besoin de recréer un bâtiment avec des caractéristiques données.
        *  Ne peut être appelé qu'une seule fois et ne devrait être appelé que pour Spawn un bâtiment
        */
        public void SetHealth(int value) {
            if (_damageTaken == 0) {
                _damageTaken = _initialHealth - value;
            }
            else {
                Debug.LogError("Health value already initialised, don't try to set the health of a building if it has already been set");
            }
        }

    }
}
