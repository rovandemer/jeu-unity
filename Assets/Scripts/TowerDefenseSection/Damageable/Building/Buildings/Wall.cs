using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Buildings {
    [RequireComponent(typeof(Building))]
    public class Wall : MonoBehaviour {

        private Building building;

        void Awake()
        {
            building = GetComponent<Building>();
            // Se tourne dos à l'objectif
            building.FlipBuildingAwayFromObjective();
        }

    }
}