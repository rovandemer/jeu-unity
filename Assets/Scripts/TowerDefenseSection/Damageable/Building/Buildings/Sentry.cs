using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Rendering.Universal;

using TowerDefense.Buildings.Components;

namespace TowerDefense.Buildings {
    [RequireComponent(typeof(Building))]
    public class Sentry : MonoBehaviour
    {

        // On récupère l'animation du bâtiment
        private Animator animator;
        private int sentryOrientation;
        private Light2D glow;

        private Color initialColor;

        private Building building;
        private ShootingZone shootingZone;
        private Damageable Target;

        private float lastShot = 0f;

        // Nombres de frames de la mitrailleuse de [-π, 0]
        private const int sentryPositions = 13;

        void Awake() {

            // On récupère l'animation du bâtiment
            this.animator = GetComponent<Animator>();
            this.shootingZone = GetComponentInChildren<ShootingZone>();

            this.glow = GetElectricityLight();
            initialColor = this.glow.color;
            this.building = GetComponent<Building>();
            
            Debug.Assert(
                this.building != null,
                $"This component needs a component of type {building.GetType()} to be attached to the same GameObject to function."
            );
            

        }

        void AttackOnCooldownEnd(Damageable target) {

            // Si le cooldown est terminé
            if (lastShot > building.AttackCooldown) {
                // On tire
                target.TakeDamage(building.AttackPower);
                lastShot = 0f;
            }
            // Sinon, on incrémente le temps depuis le dernier tir
            else {
                lastShot += Time.deltaTime;
            }
        }

        // Update est appelé à chaque frame
        void Update() {

            // On récupère l'ennemi le plus proche
            Target = shootingZone.Target();

            if (Target == null) {
                IdleAnimation();
            }
            else {
                Debug.Log($"Target : {Target}, {Target} != null");
                AttackAnimation(Target);
                AttackOnCooldownEnd(Target);
            }
        }

        private int SentryOrientation(Damageable target) {
            
            // On calcule l'angle entre la tour et l'ennemi
            float angle = Mathf.Atan2(
                target.transform.position.y - this.transform.position.y,
                target.transform.position.x - this.transform.position.x
            );

            // Et on le ramène à un nombre de ]-π, π] → ]-sentryPositions, sentryPositions] (pour pouvoir déterminer quelle position de mitrailleuse utiliser).

            // Entre -1 et 1 (environ)
            angle = angle / Mathf.PI;

            // Les positions possibles de la mitrailleuse (on va de gauche à droite, contrairement au cercle trigonométrique)
            return Mathf.CeilToInt(-sentryPositions*angle);
        }

        private void AttackAnimation(Damageable target) {
            
            animator.SetBool("IsShooting", true);

            sentryOrientation = SentryOrientation(target);
            glow.intensity = (sentryOrientation > 0) ? 0.5f : 3f;
            // Si la mitrailleuse est tournée vers le haut
            animator.SetInteger("Orientation", sentryOrientation);
            
        }

        private void IdleAnimation() {

            animator.SetBool("IsShooting", false);
            glow.intensity = 0.5f;

        }

        private Light2D GetElectricityLight() {

            foreach (Light2D light in GetComponentsInChildren<Light2D>()) {
                if (light.gameObject.name == "Electricity") {
                    return light;
                }
            }

            Debug.LogError("Pas de gameObject nommé \"Electricity\" ou pas de lumière attaché à celui-ci");
            return null;
        }

    }
}
