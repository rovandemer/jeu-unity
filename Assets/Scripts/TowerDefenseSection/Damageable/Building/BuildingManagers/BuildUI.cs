using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;


/**
 *   Classe permettant la gestion du placement de batîments.
 */
 namespace TowerDefense.Buildings.Management {
    public class BuildUI : MonoBehaviour {

        private PlayerInputActions input;
        private Canvas UI;

        [SerializeField]
        private PlacementIndicatorColorManager indicator;
        [SerializeField]
        private Image coinsIndicator;
        [SerializeField]
        private float errorWaitTime;
        private Vector3 coinOriginalScale;

        void Awake() {

            // Le RigidBody2D du joueur, permettra de définir sa position
            UI = GetComponent<Canvas>(); // On récupère l'interface utilisateur.
            UI.enabled = false;
            coinOriginalScale = coinsIndicator.transform.localScale;
        }

        void OnEnable() {
            input = Shared.Input.GameInput.inputActions;
            input.UI.Build.Enable();
            input.UI.Build.performed += OpenPlacingUI;
        }

        void OnDisable() {

            input.UI.Build.performed -= OpenPlacingUI;
            input.UI.Build.Disable();
            input = null;

        }

        /**
        *   Ouvre ou ferme le menu de placement et appelle la méthode définissant le choix de batîment.
        */
        private void OpenPlacingUI(InputAction.CallbackContext _) {

            // Si l'UI était ouverte, elle est maintenant fermée et vice-cersa
            UI.enabled = !UI.enabled;

            // On indique l'emplacement de constrution en fonction de l'ouverture de l'UI;
            indicator.gameObject.SetActive(UI.enabled);

        }

        /**
        *   Est appelé en cas d'appui sur un bouton de l'UI "BuildingSelector" et modifie la valeur en fonction.
        *   Elle indique au programme que l'utilisateur veut placer un nouveau bâtiment et stocke dans buildingNumber lequel
        *   @param chosenBulding : Le numéro du bâtiment que l'on souhaite placer.
        */
        public void GetChosenBuilding(int chosenBuilding) {

            bool buildSuccesful = BuildIfPossible(chosenBuilding);

            if (buildSuccesful) {
                // On a placé le batîment, donc on ferme l'interface
                UI.enabled = false; 
                // Et on arrête d'afficher le rectangle indiquant le placement de bâtiment
                indicator.gameObject.SetActive(false);
            }
        }

        /**
        *  Demande au BuildingManager de construire un batîment à l'identifiant demandé
        *  @returns Est-ce que le bâtiment a pu être construit ?
        */
        private bool BuildIfPossible(int buildingNumber) {

            if (BuildingManager.instance.IsBuildingTooExpensive(buildingNumber)) {
                StartCoroutine(IndicateLackOfMoney(errorWaitTime));
                return false;
            }
            
            return BuildingManager.instance.Build(buildingNumber);
        }

        /**
        *  Grossit et met en rouge l'indicateur de monnaie, indiquant un manque d'argent.
        */
        private IEnumerator IndicateLackOfMoney(float duration) {

            float t = 0;
            float normalizedT;
            while (t < duration) {

                // Valeur entre 0 et 1
                normalizedT = t/duration;

                coinsIndicator.color = Color.Lerp(Color.red, Color.white, normalizedT);
                // En applicant un Lerp sur la racine de t, le mouvement est d'abord rapide puis lent
                coinsIndicator.transform.localScale = Mathf.Lerp(2, 1, Mathf.Sqrt(normalizedT)) * coinOriginalScale;
                t += Time.deltaTime;
                yield return true;
            }

        }

    }
 }
