using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Shared.Functionnal;
using Shared.Save.Writers;
using Shared.Utils.GridExtensionMethods;

namespace TowerDefense.Buildings.Management {

    public class BuildingManager : MonoBehaviour {

        public static BuildingManager instance = null;

        public event Action<Building> BuildingPlaced;

        // Le transform sur lequel on place l'objet
        [SerializeField]
        private Transform placeOn;

        [SerializeField]
        private Building[] buildings;
        private Dictionary<string, int> nameToBuildingNumber = new Dictionary<string, int>();

        [SerializeField]
        private GameObject IllegalBuildingPositions;
        
        // Grille de toutes les positions possibles
        public Grid buildingGrid {get; private set;}
        // Position occupée de bâtiments
        private HashSet<int> illegalBuildingGridPositions = new HashSet<int>();

        int previousPosition;
        int currentPosition;

        void Awake() {
            // Rend la création de la classe unique et accessible depuis tous les scripts (BuildingManager.instance)
            if (instance != null) {
                Debug.LogError($"Second instance of {instance.GetType()} invalid.");
                Debug.LogError($"Please delete the second {instance.GetType()} from {instance.gameObject.name} or from {this.gameObject.name}");
                return;
            }
            instance = this;

            buildingGrid = GetComponentInChildren<Grid>(); // On récupère la grille des positions possibles pour les batîments

            DefineIllegalBuildingPositions();
            DefineNameToBuildingNumber();
        }

        // Méthodes appelée par Awake()

        void DefineIllegalBuildingPositions() {
            
            Transform[] childTransformations = IllegalBuildingPositions.GetComponentsInChildren<Transform>();

            int gridPosition;

            foreach (Transform childTransform in childTransformations) {
                // On veut seulement les enfants de IllegalBuildingPositions
                if (childTransform.gameObject == IllegalBuildingPositions.gameObject) { continue; }


                // On récupère la position de la grille
                gridPosition = buildingGrid.WorldToHorizontalCellPosition(childTransform.position);
                // On détruit l'objet correspondant (il ne servait qu'à indiquer une position)
                Destroy(childTransform.gameObject);
                // Et on ajoute la position au HashSet des positions illégales
                illegalBuildingGridPositions.Add(gridPosition);

            }
            // Le GameObject contenant les positions à ne pas pourvoir ne sert plus
            Destroy(IllegalBuildingPositions.gameObject);
        }

        void DefineNameToBuildingNumber() {
            for (int i = 0; i < buildings.Length; ++i) {
                nameToBuildingNumber[buildings[i].gameObject.name] = i;
            }
        }



        /**
        *  Renvoie s'il est possible pour le joueur de construire un bâtiment de numéro buildingNumber.
        *  (Cette méthode n'a pas connaissance de l'emplacement sur lequel le joueur essaye de construire.
        *  Elle ne prend donc pas en compte cette position dans son test).
        */
        public bool IsAllowedToBuild(int buildingNumber) {
            if (buildingNumber < 0 || buildingNumber >= buildings.Length) {
                Debug.LogError("Invalid building Number : " + buildingNumber);
                return false;
            }
            else if (IsBuildingTooExpensive(buildingNumber)) {
                Debug.LogError("Building too expensive");
                return false;
            }

            return true;
        }

        /**
        *   Construit le batîment demandé sur l'emplacement de placeOn.
        *   Cette méthode vérifie :
        *   1. Si le joueur a donné un bâtiment valide qu'il peut se payer
        *   2. Si l'emplacement sur lequel on essaye de construire le bâtiment est "valide" (selon EmptyBuildingSpot).

        *   Cette méthode a pour effets (en cas de réussite):
        *   1. De créer le bâtiment (à l'aide de PlaceBuilding).
        *   2. De dépenser l'argent nécessaire à la construction du bâtiment.
        *   
        *   Elle indique également visuellement au joueur si le bâtiment ne peut pas être placé sur une case donnée.
        *   Note : Elle n'indique rien au joueur s'il n'a pas l'argent pour construire le bâtiment.
        *   
        *   @param buildingNumber : Numéro identifiant le bâtiment construit.
        */
        public bool Build(int buildingNumber) {
            
            // Condition 1.
            if (!IsAllowedToBuild(buildingNumber)) {
                return false;
            }

            int buildingSpot = buildingGrid.WorldToHorizontalCellPosition(placeOn.position);
            
            Building created = null;
            // Si l'emplacement est libre (condition 2.)
            if (EmptyBuildingSpot(buildingSpot)) {
                created = PlaceBuilding(buildingNumber, buildingSpot);
            }

            BuildingPlaced?.Invoke(created);
            return created != null;
        }

        /**
        *   S'occupe de la logique derrière le placement de batiment. Le place et rajoute au position occupées l'emplacement de cosntruction.
        *   @param buildingNumber Numéro du bâtiment que l'on souhaite construire
        *   @param buildingSpot Emplacement sur la grille sur lequel on souhaite le placer
        */
        private Building PlaceBuilding(int buildingNumber, int buildingSpot) {

            Building building = InstanciateBuilding(buildingNumber, buildingGrid.CellToWorldCenter(buildingSpot, 0));
            

            // Si la construction s'est bien passée, on ajoute la position
            if (building != null) {
                illegalBuildingGridPositions.Add(buildingSpot);
                building.HasDied += BuildingDestroyed;
            }
            
            return building;
        }

        /**
         *  Méthode appelée à la mort d'un batîment.

         *  La méthode prend en paramètre un Damageable qui doit toujours être 
         *  un Building. Puisque l'interface de Damageable ne permet pas
         *  de récupérer le type précis de l'appelant, mais que cette méthode
         *  ne s'est abonnée qu'à des Building dans la méthode PlaceBuilding, 
         *  on suppose que destroyed est toujours un Building.
         */
        private void BuildingDestroyed(Damageable destroyed) {
            illegalBuildingGridPositions.Remove(
                buildingGrid.WorldToHorizontalCellPosition(
                    ((Building) destroyed).transform.position
                )
            );
        }

        /**
        *   Crée une copie de bâtiment.
        *   @param buildingNumber indice dans le tableau buildings
        *   @param position emplacement sur lequel le construire
        */
        private Building InstanciateBuilding(int buildingNumber, Vector3 position) {
            return buildings[buildingNumber].Build(position);
        }

        /**
        *  Renvoie le buildingNumber associé au bâtiment.
        *  @param buildingName Nom du GameObject du bâtiment
        */
        public int GetBuildingNumber(string buildingName) {
            return nameToBuildingNumber[buildingName];
        }

        /**
        *  Renvoie si un bâtiment est trop cher pour être construit.
        *  @param buildingNumber indice dans le tableau buildings
        */
        public bool IsBuildingTooExpensive(int buildingNumber) {
            return !Wallet.instance.CanSpendMoney(buildings[buildingNumber].Cost);
        }

        public bool EmptyBuildingSpot(Vector3 position) {
            return EmptyBuildingSpot(buildingGrid.WorldToHorizontalCellPosition(position));
        }

        /**
        *   Renvoie s'il est possible de placer un batîment sur un emplacement de la grille.
        *   
        *   @param buildingSpot L'emplacement sur lequel on essaye de construire
        */
        private bool EmptyBuildingSpot(int buildingSpot) {
            return !illegalBuildingGridPositions.Contains(buildingSpot);
        }

        /**
        *  Charge la sauvegarde des bâtiments du joueur.
        *  Est appelée à chaque chargement de la scène de TowerDefense
        */
        public void LoadBuildings() {

            BuildingData buildings = BuildingSave.Load();
            Building current;
            for (int i = 0 ; i < buildings.buildingPositions.Length ; ++i) {

                current = instance.PlaceBuilding(buildings.buildingNumbers[i], buildings.buildingPositions[i]);
                current.SetHealth(buildings.buildingHealths[i]);

            } 
        }
        /**
        *  Enregistre les bâtiments de la scène.
        *  Doit être appelé avant de changer de scène.
        *  Note : pourrait être optimisée si l'on ne fait pas un appel à FindGameObjectsWithTag 
        *         mais à la place stockerait un liste de tous les batiments construits.
        *         à voir si le temps de chargement entre la phase de rêve et la nuit est trop long.
        */
        public void SaveBuildings() {

            Debug.Log("Saving Buildings...");

            // On récupère les GameObjects correspondant au bâtiments
            GameObject[] buildingObjects = GameObject.FindGameObjectsWithTag("Building");
            if (buildingObjects.Length == 0) {
                Debug.Log("No buildings to save, no save file created");
                return;
            }

            Building[] buildings = new Building[buildingObjects.Length];
            for (int i = 0; i < buildings.Length; ++i) {
                buildings[i] = buildingObjects[i].GetComponent<Building>();
            }

            BuildingSave.Save(buildings);
        }

    }   
}
