using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Shared.Utils.GridExtensionMethods;

/**
*   Classe utilitaire pour gérer la couleur du rectangle affiché à l'utilisateur
*/
namespace TowerDefense.Buildings.Management {

    [RequireComponent(typeof(SpriteRenderer))]
    public class PlacementIndicatorColorManager : MonoBehaviour {

        [Header("Indicator colors")]

        [SerializeField]
        private Color darkErrorColor = new Color(0.5f, 0f, 0f, 0.8f);
        [SerializeField]
        private Color lightErrorColor = new Color(0.5f, 0f, 0f, 0.5f);
        [SerializeField]
        private Color emptySpaceColor = new Color(1f, 1f, 1f, 0.5f);

        [Header("Indicator placement")]

        [SerializeField]
        private Transform placeOn;

        private Coroutine current;
        private SpriteRenderer placementIndicator;

        private float currentHorizontalPosition;
        private float borderDistance;

        void Awake() {
            placementIndicator = GetComponent<SpriteRenderer>();
        }

        void Start() {

            // On écoute les placements de bâtiments.
            // Lorsqu'un bpatiment est placé, on doit mettre à jour l'indicateur
            BuildingManager.instance.BuildingPlaced += BuildingPlaced;

            // Distance entre le centre de l'indicateur et un de ses bords
            // On souhaite vérifier que le joueur a dépassé cette limite dans Update
            borderDistance = BuildingManager.instance.buildingGrid.cellSize.x / 2;
            // Initialise l'indicateur
            UpdateIndicator();
        }

        void Update() {
            // Si le joueur a quitté le placement indicator
            // On souhaite que l'indicateur soit mis à jour uniquement lorsqu'il a besoin de l'être
            if (Mathf.Abs(currentHorizontalPosition - placeOn.position.x) > borderDistance) {
                UpdateIndicator();
            }
        }

        /**
         *  Actualise l'indicateur (emplacement et couleur).
         */
        void UpdateIndicator() {
            IndicateBuildability(placeOn.position);
            this.transform.position = BuildingManager.instance.buildingGrid.CellCenter(placeOn.position);
            currentHorizontalPosition = this.transform.position.x;
        }

        private void BuildingPlaced(Building placed) {
            // Si un batîment est placé sur la case actuelle, cela signifie qu'elle est maintenant occupée
            if (placed != null) {
                IndicateError();
            }
            else {
                HighlightError();
            }
        }

        /**
         *  Change la couleur de l'indicateur en fonction de si l'emplacement placeOn est libre.
         *  La couleur indique au joueur s'il a le droit ou non de placer un batîment sur placeOn
         *  Ceci n'indique pas la possibilité effective, car il peut manquer d'argent par exemple
         */
        private void IndicateBuildability(Vector3 placeOn) {

            // S'il y a déjà un batîment sur la case placeOn', on peut pas placer de nouveau batîment, donc on montre une erreur à l'utilisateur
            if (!BuildingManager.instance.EmptyBuildingSpot(placeOn)) {
                IndicateError();
            }
            else {
                IndicateEmptySpace();
            }
        }

        /**
        *   Assombrit le SpriteRenderer en cas d'erreur dans le placement du bâtiment.
        *   @param placementIndicator Indicateur à assombrir
        */
        private IEnumerator DarkenError() {

            placementIndicator.color = darkErrorColor;
            // On rougit la zone sur laquelle l'utilisateur souhaite placer son bâtiment en rouge pendant une seconde
            yield return new WaitForSeconds(1);
            placementIndicator.color = lightErrorColor;
            current = null;

        }

        /**
        *   Assombrit le SpriteRenderer en cas d'erreur dans le placement du bâtiment
        *   si l'assombrissement n'est pas déjà en cours
        *   @param placementIndicator Indicateur à assombrir
        */
        public void HighlightError() {

            // Si l'utilisateur se retrouve dans une situation d'erreur, on veut pas lancer des coroutines inutilement.
            if (current != null) { StopCoroutine(current); }
            current = StartCoroutine(DarkenError());
            
        }

        /**
        *   Applique la couleur de fonctionnement normal à l'indicateur
        *   @param placementIndicator Indicateur à afficher avec la couleur de fonctionnement normal.
        */
        public void IndicateEmptySpace() {
            // Si on passe à la couleur blanche, on veut pas qu'elle soit accentuée
            if (current != null) { StopCoroutine(current); } 
            placementIndicator.color = emptySpaceColor;

        }

        /**
        *   Applique la couleur d'erreur à l'indicateur
        *   @param placementIndicator Indicateur à afficher avec la couleur d'erreur.
        */
        public void IndicateError() {
            placementIndicator.color = lightErrorColor;
        }

    }
}