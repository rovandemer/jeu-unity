using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TowerDefense.DayNight;

namespace TowerDefense.Moonsters.Management {
    public class WaveSpawner : MonoBehaviour {

        // Liste de tous les Moonsters que contiendra la vague
        private List<Moonster> moonsters = new List<Moonster>(); 
        private List<Moonster> spawnedMoonsters = new List<Moonster>();

        [SerializeField]
        private bool spawnWave = true;

        [SerializeField]
        private Vector2[] positions; // Points d'apparition des Moonsters

        [SerializeField]
        private int eclipseNightNumber;
        [SerializeField]
        private float maxTimeBetweenMoonsterSpawns;

        void Start() {
            if (spawnWave) {
                DayNightManager.instance.NightStarted += GenerateWave;
            }
        }

        /*
        * Calcule la difficulté d'une vague en fonction du numéro de la nuit
        * @param nightNumber Numéro de la nuit
        */
        private static int Difficulty(int nightNumber) {
            /*
            Ce calcul est arbitraire.
            Il peut être déterminé en fonction de la puissance moyenne des Moonsters.
            L'objectif est d'avoir des vagues équilibrées.
            */
            return nightNumber * 5;
        }

        /*
        * Renvoie si une nuit est de type Eclipse ou non.
        * @param nightNumber Le numéro de la nuit
        */
        private bool IsEclipse(int nightNumber) {
            /*
            Cette condition est arbitraire et pourra être changée par la suite pour rééquilibrer le jeu.
            Les nuits avec des Moonsters Eclipse pourraient fonctionner comme des boss intermédiaires pour pimenter les parties.
            Ou bien des Moonsters Eclipse peuvent apparaître aux côtés des Moonsters classiques à partir d'un certain seuil.
            */
            return nightNumber > eclipseNightNumber;
        }

        /*
        * Génère une vague de Moonsters, puis la fait apparaître à l'aide de SpawnWave.
        * La taille de la vague est en fonction d'un niveau de difficulté
        * @param nightNumber Numéro de la nuit
        * @returns Si la vague a été générée (peut être désactivé par l'attribut "spawnWave").
        */
        private void GenerateWave(int nightNumber) {

            int difficulty = Difficulty(nightNumber);
            int wavePower = 0; // On compte la puissance actuelle de la vague pour ne pas dépasser

            while (wavePower < difficulty) {

                // On demande un Monstre aléatoire
                Moonster moonster = MoonsterManager.instance.RandomMoonster(difficulty - wavePower);
                // On l'ajoute à la vague qui va être créé seulement si la wavePower n'est pas déjà trop élevée
                AddMoonsterToWave(moonster, ref wavePower);

            }

            StartCoroutine(SpawnWave());
        }

        private void AddMoonsterToWave(Moonster moonster, ref int wavePower) {

            moonsters.Add(moonster);// On ajoute le nouveau Moonster à la vague
            wavePower += moonster.Power; // On met à jour la puissance de la vague
        }

        /*
        * Fait apparaître des Moonsters quand la nuit commence
        * en fonction d'un certain niveau de difficulté
        * @param nightNumber Numéro de la nuit
        */
        private IEnumerator SpawnWave() {

            Moonster spawned;

            foreach (Moonster prefab in moonsters) {

                // On choisit un côté pour faire apparaître le Moonster.
                Vector2 startingPosition = positions[UnityEngine.Random.Range(0, positions.Length)];
                spawned = prefab.Spawn(startingPosition);
                spawned.HasDied += MoonsterHasDied;

                spawnedMoonsters.Add(spawned);

                yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f, maxTimeBetweenMoonsterSpawns));
            }

            moonsters.Clear();

        }

        /**
         *  Décrémente la variable indiquant le nombre de moonster sur le terrain
         *  Est appelé par chaque Moonster à sa mort car cette méthode est abonnée à l'événement HasDied des Moonsters
         * 
         *  La méthode prend en paramètre un Damageable qui doit toujours être 
         *  un Moonster. Puisque l'interface de Damageable ne permet pas
         *  de récupérer le type précis de l'appelant, mais que cette méthode
         *  ne s'est abonnée qu'à des Moonsters dans la méthode SpawnWave, 
         *  on suppose que deadMoonster est toujours un Moonster.
         */
        private void MoonsterHasDied(Damageable deadMoonster) {

            spawnedMoonsters.Remove((Moonster) deadMoonster);
        }

        /**
        *  Renvoie s'il y a encore un Moonster sur le terrain
        */
        public bool AreMoonstersPresent() {
            return spawnedMoonsters.Count > 0;
        }

    }
}
