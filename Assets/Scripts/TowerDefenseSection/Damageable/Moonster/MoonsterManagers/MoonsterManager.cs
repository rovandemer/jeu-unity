using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TowerDefense.Moonsters.Management {
    internal class MoonstersByPower {

        private List<Moonster> moonsters;
        public int MinPower {get; private set;}

        private static int CompareMoonsterByPower(Moonster moonster1, Moonster moonster2) {
            return moonster1.Power.CompareTo(moonster2.Power);
        }

        internal MoonstersByPower(List<Moonster> moonsters) {

            if (moonsters == null || moonsters.Count == 0) {
                Debug.LogError($"Liste invalide : {moonsters}");
                return;
            }

            this.moonsters = new List<Moonster>(moonsters);
            this.moonsters.Sort(CompareMoonsterByPower);

            MinPower = this.moonsters[0].Power;

        }

        private int FindMaxIndex(int maxPower) {
            int minIndex = 0;
            int maxIndex = moonsters.Count - 1;

            int currentIndex = maxIndex;

            bool found = false;

            // Recherche dichotomique
            while (!found && minIndex <= maxIndex) {
                currentIndex = (minIndex+maxIndex) / 2;

                if (moonsters[currentIndex].Power < maxPower) {
                    minIndex = currentIndex + 1;
                }
                else if (moonsters[currentIndex].Power > maxPower) {
                    maxIndex = currentIndex - 1;
                }
                else {
                    found = true;
                }

            }

            // +1 car on veut tous les nombres inférieurs ou égaux INCLUS
            return 1 + ((found) ? currentIndex : maxIndex);
        }

        /**
        *  On veut que MoonsterPowers[i] renvoie tous les Moonsters de puissance inférieure ou égale à i
        */
        internal List<Moonster> this[int maxPower] {
            get {

                if (maxPower < this.MinPower) {
                    return null;
                }
                return moonsters.GetRange(0, FindMaxIndex(maxPower));
            }
        }
    }

    public class MoonsterManager : MonoBehaviour {

        public static MoonsterManager instance;

        [Header("Moonsters")]

        [SerializeField]
        private List<Moonster> moonlight;

        [SerializeField]
        private List<Moonster> eclipse;

        private List<Moonster> allMoonsters;

        private MoonstersByPower currentlyAllowedMoonsters;

        private MoonstersByPower moonlightByPower;
        private MoonstersByPower allMoonstersByPower;
        
        void Awake() {
            if (instance != null) {
                Debug.LogError($"Deuxieme instance de {nameof(instance)} invalide.");
                Debug.LogError($"Veuillez supprimer ce script de {instance.gameObject.name} ou de {this.gameObject.name}");
                return;
            }
            instance = this;

            this.allMoonsters = new List<Moonster>(moonlight);
            this.allMoonsters.AddRange(eclipse);

            // MoonstersByPower des Moonlight
            this.moonlightByPower = new MoonstersByPower(this.moonlight);
            // MoonstersByPower de tous les Moonsters possibles
            this.allMoonstersByPower = new MoonstersByPower(this.allMoonsters);

            // Par défaut, on veut que les Moonlights
            SwitchToMoonlight();
        }

        public void SwitchToMoonlight() {
            currentlyAllowedMoonsters = moonlightByPower;
        }

        public void AllMoonsters() {
            currentlyAllowedMoonsters = allMoonstersByPower;
        }

        public Moonster RandomMoonster(int maxPower) {

            List<Moonster> moonstersWithPower = currentlyAllowedMoonsters[maxPower];
            return moonstersWithPower[Random.Range(0, moonstersWithPower.Count)];
        }
    }
}