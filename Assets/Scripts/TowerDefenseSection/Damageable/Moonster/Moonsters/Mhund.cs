using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TowerDefense.Moonsters.Targeters;

namespace TowerDefense.Moonsters {

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(Moonster))]
    public class Mhund : MonoBehaviour {

        private Moonster baseMoonster;
        private MoonsterTargeter targeter;
        private MhundLogic logic;
        private Damageable target;

        private Rigidbody2D rb;
        private Collider2D mhundCollider;

        [Header("Jump parameters")]
        [SerializeField]
        private float DistanceBeforeJump;
        [SerializeField]
        private float JumpChargeDuration;

        [Header("Backing off parameters")]
        
        [SerializeField]
        private Vector2 BackOffJump;
        private float BackOffJumpDuration;

        private Animator animator;

        private float timeSinceLastAttack;
        private bool isJumping = false;
        private float initialMhundOrientation;

        private Dictionary<Collider2D, Coroutine> JumpOverBuildings = new Dictionary<Collider2D, Coroutine>(8);

        void Awake() {

            baseMoonster = GetComponent<Moonster>();
            targeter = GetComponentInChildren<MoonsterTargeter>();
            
            logic = GetComponentInChildren<MhundLogic>();

            rb = GetComponent<Rigidbody2D>();
            mhundCollider = GetComponent<Collider2D>();

            animator = GetComponent<Animator>();
            BackOffJumpDuration = 2*BackOffJump.y/(-Physics2D.gravity.y)*rb.gravityScale;
        }

        void OnEnable() {
            logic.JumpOver += HandleJumping;
        }

        void OnDisable() {
            logic.JumpOver -= HandleJumping;
        }

        void Start() {
            targeter.TargetChanged += newTarget => {
                this.target = newTarget;
            };
            target = targeter.Target;
            initialMhundOrientation = baseMoonster.IsFacingLeft ? -1 : 1;
        }

        void FixedUpdate() {

            if (isJumping) {
                return;
            }
            // S'il doit se rapprocher de sa cible
            else if (baseMoonster.MoveTowardsTarget(this.target)) {
                WalkAnimation();
            }
            // Sinon, s'il est assez proche de sa cible, il l'attaque
            else if (timeSinceLastAttack > baseMoonster.AttackCooldown) {
                AttackAnimation();
                target.TakeDamage(baseMoonster.AttackPower);
                timeSinceLastAttack = 0;
            }
                
            timeSinceLastAttack += Time.fixedDeltaTime;

        }

        IEnumerator JumpAndWait(Vector2 jumpForce, float durationSeconds) {
            isJumping = true;
            JumpAnimation(true);

            rb.AddForce(jumpForce, ForceMode2D.Impulse);
            yield return new WaitForSeconds(durationSeconds);
            
            JumpAnimation(false);
            isJumping = false;
        }

        private bool IsDirectedAtMhund(Vector2 direction) {
            Debug.Log(direction);
            return Mathf.Abs(direction.x - initialMhundOrientation*transform.right.x) <= 0.3f;
        }

        private Vector2 JumpForce(float buildingHeight, out float jumpDuration) {

            /*
                On a que  x(t) = x_0 + F_x*t
                          y(t) = y_0 + F_y*t - 0.5*g*Gs*t²

                Avec x(t), y(t) des fonctions donnant respectivement la position horizontale et verticale du Mhund au cours du temps
                 (F_x, F_y) le vecteur force appliqué au départ au Mhund,
                 (x_0, y_0) sa position initiale,
                 g la constance d'accélération gravitationnelle, Gs la gravityScale, m la masse et t le temps
                
                On veut que au temps JumpDuration, le Mhund soit à la position (DistanceBeforeJump, height)

                Donc x(JumpDuration) = x_0 + DistanceBeforeJump
                  et y(JumpDuration) = y_0 + height

                Ces deux équations nous donnent les valeurs de F_x et F_y et JumpDuration suivantes :
             */

            buildingHeight *= 1.3f;

            jumpDuration = Mathf.Sqrt(2*buildingHeight/(-Physics2D.gravity.y*rb.gravityScale));

            Vector2 v = rb.mass*new Vector2(
                2*DistanceBeforeJump/jumpDuration * ((baseMoonster.IsFacingLeft) ? -1 : 1),
                buildingHeight/jumpDuration + 0.5f*(-Physics2D.gravity.y)*rb.gravityScale*jumpDuration
            );

            return v;

        }

        void HandleJumping(Collider2D building) {

            // Si le Moonster essaye déjà de sauter au dessus de quelque chose
            // TODO : Dictionary

            if (JumpOverBuildings.ContainsKey(building)) {
                Debug.Log($"Discarded jump over: {building}");
                return;
            }

            float buildingHeight = building.bounds.size.y;
            Coroutine jumpOverBuilding = StartCoroutine(JumpOnceArrived(building, buildingHeight));

            JumpOverBuildings[building] = jumpOverBuilding;
        }

        IEnumerator JumpOnceArrived(Collider2D building, float height) {

            float jumpDuration;

            yield return new WaitUntil( 
                () => !isJumping && mhundCollider.Distance(building).distance <= DistanceBeforeJump
            );
            
            isJumping = true;
            JumpAnimation(true);
            
            rb.velocity = Vector2.zero;

            yield return new WaitForSeconds(JumpChargeDuration);
            rb.AddForce(JumpForce(height, out jumpDuration), ForceMode2D.Impulse);

            yield return new WaitForSeconds(jumpDuration);
            JumpOverBuildings.Remove(building);

            isJumping = false;
            JumpAnimation(false);

        }

        void OnCollisionEnter2D(Collision2D collision) {
            // Si le Mhund s'est cogné la tête en avançant, il sautera en arrière pour ne pas rester coincé contre un mur
            if (IsDirectedAtMhund(collision.GetContact(0).normal)) {
                StartCoroutine(BackOff());
                
            }
        }

        IEnumerator BackOff() {

            yield return new WaitWhile(
                () => isJumping
            );

            Debug.Log("Backing off...");

            Vector2 backOffJumpForce = rb.mass*BackOffJump;
            if (!baseMoonster.IsFacingLeft) {
                backOffJumpForce.x = -backOffJumpForce.x;
            }
            yield return JumpAndWait(backOffJumpForce, BackOffJumpDuration);
            
        }

        void WalkAnimation() {
            animator.SetBool("IsAttacking", false);
        }

        void AttackAnimation() {
            animator.SetBool("IsAttacking", true);
        }

        void JumpAnimation(bool isJumping) {
            animator.SetBool("IsJumping", isJumping);
        }

    }
}