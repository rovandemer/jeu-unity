using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Moonsters {
    public class Moonster : MonoBehaviour, Damageable {

        [Header("Damageable attributes")]
        [SerializeField]
        private Sprite _healthBarIcon;
        public Sprite HealthBarIcon {
            get => _healthBarIcon;
        }

        [SerializeField]
        private int _initialHealth;
        private int _damageTaken = 0;
        public int Health {
            get => _initialHealth - _damageTaken;
        }

        private bool isTakingDamage = false;
        public event Action<Damageable> HasDied;
        public Collider2D hitbox {get; private set;}

        [Header("Moonster attributes")]
        [SerializeField]
        private int _power;
        public int Power {
            get => _power;
        }

        [SerializeField, Range(0, 20)]
        private float _speed;
        public float Speed{
            get => _speed;
        }

        [SerializeField]
        private float _attackRange;
        public float AttackRange {
            get => _attackRange; 
        }

        [SerializeField]
        private int _attackPower;
        public int AttackPower{
            get => _attackPower;
        }

        [SerializeField]
        private float _attackCooldown;
        public float AttackCooldown {
            get => _attackCooldown;
        }

        private SpriteRenderer gameSpriteRenderer;

        float dampVelocity = 0f;
        float smoothTime = 0.1f;

        public bool IsFacingLeft {get; private set;} = true;
        
        int buildingLayer;

        

        void Awake() {

            buildingLayer = LayerMask.NameToLayer("Building");
            hitbox = GetComponent<Collider2D>();
            gameSpriteRenderer = GetComponent<SpriteRenderer>();
        }


        public Moonster Spawn(Vector3 position) {

            return Instantiate(this, position, Quaternion.identity);

        }

        /**
        *  Bouge en direction du Damageable target si c'est nécessaire pour attaquer.
        *  Si le Moonster est à une distance inférieure à AttackRange de target, alors il ne bouge pas (il est assez proche) et cette méthode renvoie faux.
        */
        public bool MoveTowardsTarget(Damageable target) {

            Face(target);
            if (target.hitbox.Distance(this.hitbox).distance < this.AttackRange) {
                return false;
            }

            Vector2 currentVelocity = hitbox.attachedRigidbody.velocity;
            currentVelocity.x = Mathf.SmoothDamp(currentVelocity.x, (IsFacingLeft) ? -Speed: Speed, ref dampVelocity, smoothTime, maxSpeed: 25f);
            hitbox.attachedRigidbody.velocity = currentVelocity;
            return true;
        }

        public void Face(Damageable target) {
            if (this.IsFacingLeft && target.transform.position.x > this.transform.position.x
            || !this.IsFacingLeft && target.transform.position.x < this.transform.position.x) {
                Flip();
            }
        }

        void Flip() {

            this.IsFacingLeft = !this.IsFacingLeft;
            this.transform.Rotate(Vector3.up, 180f);

        }

        IEnumerator IndicateDamage() {
            isTakingDamage = true;
            this.gameSpriteRenderer.color = Color.magenta;
            yield return new WaitForSeconds(0.1f);
            this.gameSpriteRenderer.color = Color.white;
            isTakingDamage = false;
        }

        public void TakeDamage(int damage) {
            
            _damageTaken += damage;
            if (!isTakingDamage) {
                StartCoroutine(IndicateDamage());
            }
            if (Health < 0) {
                Die();
            }
        }

        public void Die() {
            HasDied?.Invoke(this);
            // Il sera uniquement détruit en fin de boucle Update, les classes abonnées à l'événement HasDied ne reçevront pas null
            Destroy(this.gameObject);
        }

        void OnCollisionEnter2D(Collision2D collider) {

            // Si le Moonster s'est pris un bâtiment sur la tête
            if (collider.gameObject.layer == buildingLayer && collider.GetContact(0).normal.y < 0.5) {
                // Alors il meurt et le bâtiment "rebondit" sur lui
                collider.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 3000));
                Die();
            }

        }

    }
}
