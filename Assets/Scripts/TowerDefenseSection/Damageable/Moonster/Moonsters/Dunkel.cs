using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Moonsters {
    [RequireComponent(typeof(Moonster))]
    public class Dunkel : MonoBehaviour {

        Moonster baseMoonster;
        private Targeters.MoonsterTargeter moonsterTargeter;

        private float timeSinceLastDamage = 0f;
        
        private Damageable target = null;
        private Animator animator;

        private Collider2D moonsterCollider;

        void Awake() {
            
            animator = GetComponent<Animator>();
            this.baseMoonster = GetComponent<Moonster>();
            this.moonsterTargeter = GetComponentInChildren<Targeters.MoonsterTargeter>();
            this.moonsterCollider = GetComponent<Collider2D>();
            animator.enabled = false;

        }

        void Start() {
            this.target = moonsterTargeter.Target;

            moonsterTargeter.TargetChanged += newTarget => {
                this.target = newTarget;
            };
        }

        void FixedUpdate() {

            if (baseMoonster.MoveTowardsTarget(target)) {
                WalkAnimation();
            }
            else if (timeSinceLastDamage > baseMoonster.AttackCooldown) {
                AttackAnimation();
                target.TakeDamage(baseMoonster.AttackPower);
                timeSinceLastDamage = 0;
            }

            timeSinceLastDamage += Time.fixedDeltaTime;
        }

        void AttackAnimation() {
            animator.enabled = false;
            baseMoonster.Face(this.target);
        }

        void WalkAnimation() {
            animator.enabled = true;
        }

    }
}
