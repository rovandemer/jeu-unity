using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TowerDefense.Buildings;

namespace TowerDefense.Moonsters.Targeters {

    public class MhundLogic : MonoBehaviour, MoonsterLogic {

        Damageable Objective;
        Collider2D objectiveCollider;
        Damageable Player;
        Collider2D playerCollider;

        [SerializeField]
        private Transform feetCheck;

        private int groundMask;

        public event Action<Collider2D> JumpOver;

        void Start() {

            Objective = FindAnyObjectByType<TowerDefense.ObjectiveNS.Objective>() as Damageable;
            objectiveCollider = Objective.hitbox;
            Player = FindAnyObjectByType<TowerDefense.PlayerLogic.Player>() as Damageable;
            playerCollider = Player.hitbox;

            groundMask = LayerMask.GetMask("Default");
        }

        /**
         *  Renvoie la cible prioritaire du Moonster parmi la liste de Collider2D donnés
         */
        public Collider2D PrioritizeTarget(List<Collider2D> potentialTargets) {

            Collider2D highestPriorityTarget = null;

            foreach (Collider2D potentialTarget in potentialTargets) {

                if (potentialTarget == objectiveCollider) {
                    highestPriorityTarget = objectiveCollider;
                    break;
                }
                else if (potentialTarget == playerCollider) {
                    highestPriorityTarget = playerCollider;
                }
                // Le Mhund s'en fout des batîments
            }

            return highestPriorityTarget ?? objectiveCollider;
        }

        private bool IsTouchingGround() {
            return Physics2D.OverlapCircle(feetCheck.position, 0.3f, groundMask) != null;
        }

        public Collider2D HandleBlocking(Collider2D preferedTarget, Collider2D blocking) {
            
            if (preferedTarget != blocking && IsTouchingGround()) {

                // Il sautera au dessus de l'obstacle le bloquant
                JumpOver?.Invoke(blocking);
                // Et sa cible préférée est conservée
                return preferedTarget;
            }

            // Sinon, s'il est déjà en l'air (ou sur un bâtiment), alors sa cible est ce qui le bloque
            return blocking;
        }

    }
}

