using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Moonsters.Targeters {

    [RequireComponent(typeof(CircleCollider2D))]
    [RequireComponent(typeof(MoonsterLogic))]
    public class MoonsterTargeter : MonoBehaviour {
        
        MoonsterLogic moonsterLogic;

        public event Action<Damageable> TargetChanged;
        private Damageable _target;
        public Damageable Target {
            get => _target;
            private set {

                // Si la cible a changé, comme ça on est sûr que TargetChanged est appelé à chaque changement de cible
                if (_target != value) {

                    _target = value;
                    TargetChanged?.Invoke(_target);
                }

            }
        }
        
        private Damageable Objective;
        private Collider2D objectiveCollider;
        
        public Damageable Player;
        private Collider2D playerCollider;

        // Zone de détection des ennemis du Moonster
        private CircleCollider2D targettingRange;
        private int moonsterTargetsMask;
        private ContactFilter2D moonsterTargetsFilter;
        private List<Collider2D> potentialTargets = new List<Collider2D>(5);

        void Awake() {

            this.moonsterLogic = GetComponent<MoonsterLogic>();
            this.targettingRange = GetComponent<CircleCollider2D>();

            // Initialisation du filtre déterminant les ennemis du Moonster
            this.moonsterTargetsFilter = new ContactFilter2D();
            this.moonsterTargetsFilter.useLayerMask = true;
            // Masque utilisant toutes les cibles potentielles du Moonster
            this.moonsterTargetsMask = LayerMask.GetMask("Building", "Player", "Objective");
            this.moonsterTargetsFilter.layerMask = this.moonsterTargetsMask;

            // On veut que notre collider n'interagisse qu'avec les couches Building, Player et Objective
            this.gameObject.layer = LayerMask.NameToLayer("Detect Moonster Target");
        }

        void Start() {

            Objective = FindAnyObjectByType<TowerDefense.ObjectiveNS.Objective>() as Damageable;
            objectiveCollider = Objective.hitbox;
            Player = FindAnyObjectByType<TowerDefense.PlayerLogic.Player>() as Damageable;
            playerCollider = Player.hitbox;

            this.Target = Objective;
            
        }

        /**
         *  Cherche à déterminer la cible prioritaire du Moonster, 
         *  appelle HandleMovingTarget pour s'occuper du fait que la cible est mobile
         *  et renvoie le Damageable qui empêche l'accès à la cible prioritaire.
         *
         *  /!\ Renvoie jamais null sauf si jamais tous les objets de la scène sont détruit et que cette méthode est appelée dans On
         */
        private Collider2D FindReachableTarget() {

            targettingRange.OverlapCollider(moonsterTargetsFilter, potentialTargets);
            Collider2D highestPriorityTarget = moonsterLogic.PrioritizeTarget(potentialTargets);
            potentialTargets.Clear();

            // hightestPriorityTarget est null seulement si moonsterLogic a mal implémenté PrioritizeTarget
            // Ou que highestPriorityTarget est en fait un objet détruit cette frame, signifiant que l'on change de scène
            if (highestPriorityTarget == null) {
                return highestPriorityTarget;
            }

            HandleMovingTarget(highestPriorityTarget);

            return moonsterLogic.HandleBlocking(highestPriorityTarget, Blocking(highestPriorityTarget));
        }

        /**
         *  Maintient à jour la cible si jamais target est une cible mobile
         */
        private void HandleMovingTarget(Collider2D target) {

            if (target != playerCollider) {
                return;
            }
            StartCoroutine(FollowMovingTarget(target));

        }

        /**
         *  Assigne la véritable cible du Moonster au Targeter une fois que la voie est libre pour l'atteindre
         *  Si la cible a changé depuis l'appel à la Coroutine, alors cette Coroutine s'arrête sans effet sur la référence sur la cible
         *  (La Coroutine suit la cible mobile, si la cible mobile n'est plus la cible du Moonster, alors la Coroutine n'a plus lieu d'être.)
         *
         *  Cette méthode suppose que actualTarget est une cible mobile
         */
        private IEnumerator FollowMovingTarget(Collider2D actualTarget) {

            // TODO : Trouver une optimisation, blocking ne peut être que actualTarget ou Target au départ, stocker ces deux ?
            
            Collider2D blocking = null;
            do {

                blocking = moonsterLogic.HandleBlocking(actualTarget, Blocking(actualTarget));
                // On récupère le Damageable uniquement s'il est différent
                if (Target.hitbox != blocking) {
                    Target = blocking.GetComponent<Damageable>();    
                }
                yield return false;
                
            // Les coroutines sont executés après OnTriggerEnter/Exit et Update
            // Donc on a assurance que Target n'a pas été modifié cette frame et donc que si cette boucle est executé alors
            // cela signifie que le Target que l'on assigne est bien le target demandé
            } while (Target.hitbox == actualTarget || Target.hitbox == blocking);

        }


        private void OnTriggerEnter2D() {
            Target = FindReachableTarget().transform.GetComponent<Damageable>();
        }

        // Appelé lorsque la cible sort du champ ou qu'elle meurt
        private void OnTriggerExit2D(Collider2D collider) {

            // On est en train de quitter la scène, on ignore ce passage dans OnTriggerExit2D() (sinon, on récupère une erreur de compilation)
            if (Target == null || collider == null) {
                return;
            }

            // Il faut changer de cible si la cible actuelle est celle qui vient de quitter la targetingRange
            if (Target.transform == collider.transform) {

                Collider2D reachableTarget = FindReachableTarget();
                // OnTriggerExit2D est (parfois) appelée en cas de déchargement de la scène, FindReachableTarget renvoie alors
                // La valeur assignée à Target est alors sans importance, cet objet sera détruit la frame prochaine
                if (reachableTarget != null) {
                    Target = reachableTarget.transform.GetComponent<Damageable>();
                }
            }
        }

        /**
         *  Renvoie le Transform qui se trouve entre le centre du Targeter et target
         *  Permet de savoir si la cible est directement atteignable par le Moonster.
         *  Renvoie target si rien n'a été touché
         */
        public Collider2D Blocking(Collider2D target) {

            // Direction dans laquelle se trouve la cible
            Vector2 direction = target.transform.position - this.transform.position;

            RaycastHit2D hit = Physics2D.Raycast(
                this.transform.position, direction,
                // On veut trouver le Damageable qui bloque l'accès à target
                // Donc, sur une distance de longueur la zone de visée du Moonster, on cherche Batiments, Joueur ou Objectif
                distance: this.targettingRange.radius+1,
                layerMask: this.moonsterTargetsMask
            );

            return (hit.collider == null) ? target : hit.collider;
        }
    }
}
