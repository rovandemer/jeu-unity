using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Passer un objet "Prioritize" qui s'occupe de choisir le préféré parmi une liste de Collider2D

namespace TowerDefense.Moonsters.Targeters {

    public class DunkelLogic : MonoBehaviour, MoonsterLogic {

        Damageable Objective;
        Collider2D objectiveCollider;
        Damageable Player;
        Collider2D playerCollider;

        void Start() {

            Objective = (Damageable) FindAnyObjectByType<TowerDefense.ObjectiveNS.Objective>();
            objectiveCollider = Objective.hitbox;
            Player = (Damageable) FindAnyObjectByType<TowerDefense.PlayerLogic.Player>();
            playerCollider = Player.hitbox;
        }


        /**
         *  Renvoie la cible prioritaire du Moonster parmi la liste de Collider2D donnés
         */
        public Collider2D PrioritizeTarget(List<Collider2D> potentialTargets) {

            if (potentialTargets.Count == 0) {
                return objectiveCollider;
            }

            Collider2D highestPriorityTarget = null;

            /**
             *  On souhaite implémenter la logique suivante :
             *  1. Si l'objectif est visible par le monstre, cet objectif est la cible
             *  2. Sinon si le joueur est visible, le joueur est la cible.
             *  3. Sinon, sa cible est le batîment le plus proche de l'objectif
             */

             Debug.Log($"Potential targets : [{string.Join(",", potentialTargets)}]");

            foreach (Collider2D potentialTarget in potentialTargets) {
                if (potentialTarget == objectiveCollider) {
                    highestPriorityTarget = objectiveCollider;
                    // Il n'existe pas de cible plus importante que l'objectif
                    break;
                }
                else if (potentialTarget == playerCollider) {
                    highestPriorityTarget = playerCollider;
                }
                else if (highestPriorityTarget != playerCollider) {
                    highestPriorityTarget = objectiveCollider.Closest(
                        highestPriorityTarget, potentialTarget
                    );
                }
            }

            Debug.Log($"HightestPriorityTarget : {highestPriorityTarget}");

            return highestPriorityTarget;
        }

        public Collider2D HandleBlocking(Collider2D preferedTarget, Collider2D blocking) {
            return blocking;
        }

    }




    /**
     *  Méthode d'extension pour trouver le Transform le plus proche parmi deux transforms
     */
    internal static class ColliderExtensions {

        internal static Collider2D Closest(this Collider2D point, Collider2D c1, Collider2D c2) {

            if (c1 == null) {
                return c2;
            }
            if (c2 == null) {
                return c1;
            }

            float distanceT1 = Mathf.Abs(c1.transform.position.x - point.transform.position.x);
            float distanceT2 = Mathf.Abs(c2.transform.position.x - point.transform.position.x);

            return (distanceT1 < distanceT2) ? c1 : c2;

        }
    }

}

        
