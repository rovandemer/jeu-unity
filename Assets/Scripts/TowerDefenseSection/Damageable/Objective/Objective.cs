using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.ObjectiveNS {
    public class Objective : MonoBehaviour, Damageable {

        [Header("Damageable attributes")]
        [SerializeField]
        private Sprite _healthBarIcon;
        public Sprite HealthBarIcon {
            get => _healthBarIcon;
        }

        [SerializeField]
        private int _initialHealth;
        private int _damageTaken = 0;
        public int Health {
            get => _initialHealth - _damageTaken;
        }

        private bool isTakingDamage = false;
        public event Action<Damageable> HasDied;
        public Collider2D hitbox {get; private set;}

        private Animator animator;
        private SpriteRenderer objectiveSprite;

        void Awake() {
            animator = GetComponent<Animator>();
            hitbox = GetComponent<Collider2D>();
            objectiveSprite = GetComponent<SpriteRenderer>();
        }


        IEnumerator IndicateDamage() {
            isTakingDamage = true;
            this.objectiveSprite.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            this.objectiveSprite.color = Color.white;
            isTakingDamage = false;
        }

        public void TakeDamage(int damage) {
            
            _damageTaken += damage;
            if (!isTakingDamage) {
                StartCoroutine(IndicateDamage());
            }
            if (Health < 0) {
                Die();
            }

            animator.SetFloat("HealthRatio", (float) Health/_initialHealth);
        }

        public void Die() {
            HasDied?.Invoke(this);
        }

    }
}
