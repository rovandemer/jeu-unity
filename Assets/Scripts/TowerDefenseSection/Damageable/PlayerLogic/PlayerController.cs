using UnityEngine;
using UnityEngine.InputSystem;

namespace TowerDefense.PlayerLogic {

    public class PlayerController : MonoBehaviour {

        [Header("Player charactersitics")]
        [SerializeField]
        private float Speed = 40f;
        [SerializeField]
        private float JumpForce = 400f; // Force de saut du personnage
        
        [Header("Ground check")]
        [SerializeField]
        private LayerMask Ground; // Détermine ce qui est considéré comme du sol
        [SerializeField]
        private Transform GroundCheck; // Position de vérification du sol
        private float groundedRadius = 0.2f; // Rayon du cercle de vérification du sol, permet de déterminer si le personnage est au sol
        private bool isGrounded = false; // Détermine si le personnage est au sol


        private Rigidbody2D rb; // Rigidbody du personnage
        public bool IsFacingRight {get; private set;} = false; // Détermine si le personnage regarde à droite

        // On va définir les variables pour un lerp sur la vitesse du personnage (on veut que le joueur accélère en 1s)
        private float smoothVelocity = 0f;
        private float smoothTime = 0.3f;

        PlayerInputActions input;

        private void Awake() {
            rb = GetComponent<Rigidbody2D>();
        }

        void OnEnable() {

            input = Shared.Input.GameInput.inputActions;
        
            input.Player.HorizontalMovement.Enable();
            input.Player.Jump.Enable();
            input.Player.Jump.performed += Jump;

        }

        void OnDisable() {

            input.Player.Jump.performed -= Jump;
            input.Player.Jump.Disable();
            input.Player.HorizontalMovement.Disable();

            input = null;
        }

        private void FixedUpdate() {

            if (input != null) {
                Move( input.Player.HorizontalMovement.ReadValue<float>() );
            }

            // Vérifie si le personnage est au sol
            isGrounded = false;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, groundedRadius, Ground);

            for (int i = 0; i < colliders.Length; i++) {
                if (colliders[i].gameObject != gameObject) {
                    isGrounded = true;
                }
            }
        }

        /**
        *  Se déplace sur l'axe horizontal selon la valeur de move
        */
        public void Move(float move) {

            // Déplacement du personnage, avec un lerp pour un déplacement plus fluide (Lerp)
            // Une seconde pour atteindre la vitesse de déplacement
            float targetVelocityX = move * Speed;
            float smoothVelocityX = Mathf.SmoothDamp(rb.velocity.x, targetVelocityX, ref smoothVelocity, smoothTime);
            rb.velocity = new Vector2(smoothVelocityX, rb.velocity.y);

            // On tourne le personnage s'il va dans la direction opposée à celle où il regarde
            if (move > 0 && !IsFacingRight) {
                Flip();
            } else if (move < 0 && IsFacingRight) {
                Flip();
            }
            
        }

        public void Jump(InputAction.CallbackContext context) {

            if (isGrounded) {
                // Si le personnage est au sol et qu'il doit sauter, il saute
                isGrounded = false;
                rb.AddForce(new Vector2(0f, JumpForce));
            }
        }

        /**
        *  Tourne le personnage.
        */
        private void Flip() {
            // Inverse la valeur de FacingRight
            IsFacingRight = !IsFacingRight;

            this.transform.Rotate(Vector3.up, 180f);

        }
    }
}