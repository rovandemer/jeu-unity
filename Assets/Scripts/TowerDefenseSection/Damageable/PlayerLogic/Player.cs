using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.PlayerLogic {
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Collider2D))]
    public class Player : MonoBehaviour, Damageable {
        
        private SpriteRenderer gameSpriteRenderer;

        [Header("Damageable attributes")]
        [SerializeField]
        private Sprite _healthBarIcon;
        public Sprite HealthBarIcon {
            get => _healthBarIcon;
        }

        [SerializeField]
        private int _initialHealth;
        private int _damageTaken = 0;
        public int Health {
            get => _initialHealth - _damageTaken;
        }

        private bool isTakingDamage = false;
        public event Action<Damageable> HasDied;
        private Rigidbody2D rb;
        public Collider2D hitbox {get; private set;}

        [Header("Player attributes")]

        private Animator playerAnimator;

        void Awake() {
            playerAnimator = GetComponent<Animator>();
            hitbox = GetComponent<Collider2D>();
            rb = hitbox.attachedRigidbody;
            gameSpriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Update() {
            WalkAnimation();
        }

        void WalkAnimation() {
            // L'animation de marche est activé si le déplacement horizontal dépasse 0.1, désactivé sinon
            playerAnimator.SetBool("IsWalking", Mathf.Abs(rb.velocity.x) > 0.1);
        }

        IEnumerator IndicateDamage() {
            isTakingDamage = true;
            this.gameSpriteRenderer.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            this.gameSpriteRenderer.color = Color.white;
            isTakingDamage = false;
        }

        public void TakeDamage(int damage) {
            
            _damageTaken += damage;
            if (!isTakingDamage) {
                StartCoroutine(IndicateDamage());
            }
            if (Health < 0) {
                Die();
            }
        }

        public void Die() {
            HasDied?.Invoke(this);
        }

    }
}