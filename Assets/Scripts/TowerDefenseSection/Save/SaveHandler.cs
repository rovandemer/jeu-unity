using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using TowerDefense.DayNight;

/**
 *  S'occupe de charger et enregistrer quand il faut les données sur le jeu.
 *  NOTE : Enregistre la scène à chaque fois que la TowerDefenseSection est chargée
 */

namespace TowerDefense.Save {
    public class SaveHandler : MonoBehaviour {

        [Header("Save on events from")]
        [SerializeField]
        DayNightManager dayNightManager;

        void Start() {
            dayNightManager.StartDay += Shared.Save.SaveHandler.SaveGameDay;
        }

    }
}