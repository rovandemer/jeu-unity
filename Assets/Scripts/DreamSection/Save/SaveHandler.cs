using System;
using System.Collections.Generic;
using UnityEngine;

using Dream.Functionnal;
/**
 *  S'occupe de charger et enregistrer quand il faut les données sur le jeu.
 *  NOTE : Enregistre la scène à chaque fois que la TowerDefenseSection est chargée
 */

namespace Dream.Save {
    public class SaveHandler : MonoBehaviour {

        [Header("Save on events from")]
        [SerializeField]
        Timer timer;

        void Start() {
            timer.WakingUp += Shared.Save.SaveHandler.SaveGameNight;
        }
    }
}
