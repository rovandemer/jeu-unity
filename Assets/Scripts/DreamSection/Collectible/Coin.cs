using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dream.Collectible {

    public class Coin : MonoBehaviour {
        
        [SerializeField, Range(1, 100)]
        private int coinMaxValue = 1;

        private int coinValue;

        void Awake()
        {
            coinValue = Random.Range(1, coinMaxValue);
        }

        public int GetCoin()
        {
            Destroy(gameObject);
            return coinValue;
        }

        void OnTriggerEnter2D() {
            Shared.Functionnal.
                Wallet.instance.GetCoin(this);
        }

    }

}