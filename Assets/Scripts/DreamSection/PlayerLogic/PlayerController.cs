using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/**
 *  Classe semblable à {@link TowerDefense.PlayerLogic.PlayerController2D}.
 */
namespace Dream.PlayerLogic {

    public class PlayerController : MonoBehaviour {

        private Transform playerTransform;
        private Rigidbody2D rb;
        private Animator playerAnimation;

        [SerializeField]
        private float playerSpeed;

        bool IsFacingRight = false;

        PlayerInputActions input;

        Vector2 MoveDirection = Vector2.zero;

        void OnEnable() {
            input = Shared.Input.GameInput.inputActions;

            input.Player.HorizontalMovement.Enable();
            input.Player.VerticalMovement.Enable();
        }

        void OnDisable() {
            
            input.Player.HorizontalMovement.Disable();
            input.Player.VerticalMovement.Disable();

            input = null;
        }

        // Start is called before the first frame update
        void Start() {
            playerTransform = GetComponent<Transform>();
            rb = GetComponent<Rigidbody2D>();
        }

        void FixedUpdate() {
            MoveDirection = (new Vector2(
                input.Player.HorizontalMovement.ReadValue<float>(),
                input.Player.VerticalMovement.ReadValue<float>()
            )).normalized;
            
            Move(MoveDirection);
        }

        /**
        *  Déplace le joueur selon le mouvement horizontal et vertical passé en paramètre.
        */
        private void Move(Vector2 direction) {
            // Normalisé pour que le joueur ne soit pas plus rapide en diagonale
            rb.velocity = playerSpeed*direction;

            // Si le personnage veut bouger dans la direction opposée à son orientation actuelle, on le tourne
            if (direction.x > 0 && !IsFacingRight) {
                Flip();
            }
            else if (direction.x < 0 && IsFacingRight) {
                Flip();
            }
        }

        private void Flip() {
            // Inverse la valeur de FacingRight
            IsFacingRight = !IsFacingRight;
            this.transform.Rotate(Vector3.up, 180f);
        }
    }
}