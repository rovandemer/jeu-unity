using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dream.PlayerLogic {
    public class CameraPosition : MonoBehaviour {

        [SerializeField]
        private Player player;

        // Update is called once per frame
        void Update() {

            this.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, 0);
        }
    }
}
