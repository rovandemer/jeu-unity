using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dream.PlayerLogic {

    public class Player : MonoBehaviour {

        CharacterController controller;
        Animator playerAnimation;
        Functionnal.Timer timer;

        int defaultLayer;


        Rigidbody2D rb;
        bool isFacingUp;

        void Start() {
            controller = GetComponent<CharacterController>();
            playerAnimation = GetComponent<Animator>();
            defaultLayer = LayerMask.NameToLayer("Default");

            rb = GetComponent<Rigidbody2D>();
        }

        void Update() {
            WalkAnimation();
        }

        void WalkAnimation() {
            // Envoie l'information du mouvement du personnage
            playerAnimation.SetFloat("Move", rb.velocity.sqrMagnitude);
            // Si l'input vertical est positif, alors il regarde vers le haut
            playerAnimation.SetBool("IsFacingUp", IsFacingUp());
        }

        bool IsFacingUp() {
            // Si le personnage regarde vers le haut, il se retourne que lorsqu'il va clairement vers le bas
            // Ceci évite qu'il se retourne à chaque fois qu'il ne bouge pas
            if (isFacingUp) {
                isFacingUp = rb.velocity.y > -0.1;
            }
            else {
                isFacingUp = rb.velocity.y > 0.1;
            }
            return isFacingUp;
        }

        void OnCollisionEnter2D(Collision2D collision) {

            // Si la collision s'est juste faite avec un mur, on l'ignore
            if (collision.collider.gameObject.layer == defaultLayer) {
                return;
            }
            // Sinon, c'est que le joueur s'est fait toucher par un ennemi, il se réveille alors
            else {
                timer.StartWakeUp();
            }
        }

    }
}