using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.Universal;

using static Shared.Utils.Time;

namespace Dream.Functionnal {

    public class Timer : MonoBehaviour {

        public event Action WakingUp;

        [SerializeField]
        private float SecondsAllowedInDreamWorld;

        [Header("Wake up handling")]
        [SerializeField]
        private Light2D SceneLight;
        [SerializeField]
        private float WakeUpDuration;
        [SerializeField]
        private float WakeUpStrengh = 0.002f;

        [Header("Warning")]
        [SerializeField]
        private float[] Warnings;
        [SerializeField]
        float WarningDuration;
        [SerializeField, Range(0, 1f)]
        float WarningStrengh;
        [SerializeField]
        private Camera cam;

        float initialCamDepth;
        float initialIntensity;

        // Start is called before the first frame update
        void Awake() {

            initialCamDepth = cam.transform.position.z;
            initialIntensity = SceneLight.intensity;

            // On lance des coroutines qui s'occupent de lancer les Warnings lorsqu'il y en a besoin
            foreach (float warningTime in Warnings) {
                StartCoroutine(WithDelay(this, ShowWarning(), warningTime));
            }

            StartCoroutine(WithDelay(this, WakeUp(), SecondsAllowedInDreamWorld));
        }

        /**
        *  Fait secouer la caméra pour une durée et force donnés en paramètre.
        */
        IEnumerator ShowWarning() {

            yield return StartCoroutine(ExecuteEachFrame(ShakeCamera, WarningDuration));

            // Remise à 0 de la position de la caméra
            cam.transform.localPosition = new Vector3(0, 0, initialCamDepth);

        }

        void ShakeCamera() {
            float x = UnityEngine.Random.Range(-1f, 1f) * WarningStrengh;
            float y = UnityEngine.Random.Range(-1f, 1f) * WarningStrengh;
            cam.transform.localPosition = new Vector3(x, y, initialCamDepth);
        }

        /**
        *  Illumine l'écran jusqu'à "réveiller" le joueur, c'est-à-dire, passer à la scène de TowerDefense
        */
        IEnumerator WakeUp() {

            WakingUp?.Invoke();
            Time.timeScale = 0f;

            AsyncOperation sceneLoad = SceneManager.LoadSceneAsync("TowerDefenseSection");

            // La scène se charge "trop rapidement", on perd la transition lumineuse
            // On force le temps de chargement à au moins être aussi long que WakeUpDuration
            sceneLoad.allowSceneActivation = false;

            // On monte la luminosité tant que sceneLoad n'a pas fini de charger la scène
            yield return StartCoroutine(ExecuteEachFrame(() => {
                SceneLight.intensity += WakeUpStrengh;
            }, WakeUpDuration, true));
            
            Time.timeScale = 1f;

            sceneLoad.allowSceneActivation = true;
            
        }

        public void StartWakeUp() {
            // Si le joueur meurt par la suite dans le monde réel, il conserve ses statistiques du monde de rêve
            StartCoroutine(WakeUp());
        }

    }

}
