using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Représente une grotte
 */

namespace Dream.MapManagement {
    public class Cave : MonoBehaviour 
    {

        // Config de la taille max de la grotte
        [SerializeField]
        public int caveWidth = 15;
        [SerializeField]
        public int caveHeight = 10;

        // Config de son taux de remplissage
        [SerializeField, Range(0, 1)]
        private float minFillRate = 0.5f;
        [SerializeField, Range(0, 1)]
        private float maxFillRate = 0.5f;

        [SerializeField, Range(0, 10)]
        private int smoothIterations = 5;

        [SerializeField, Range(1, 10)]
        private int tunnelSize = 3;

        [SerializeField, Range(0, 10)]
        private int tunnelVariation = 3;

        [SerializeField, Range(0, 100)]
        private int caveThreshold = 10;

        // La map de la grotte
        private int[,] caveMap;
        private List<Room> rooms = new List<Room>();

        private List<Vector2Int> directions = new List<Vector2Int>()
        {
            new Vector2Int(0, 1),
            new Vector2Int(0, -1),
            new Vector2Int(1, 0),
            new Vector2Int(-1, 0)
        };

        private float fillRate
        {
            get
            {
                return Random.Range(minFillRate, maxFillRate);
            }
        }

        /**
        * Génère une map de grotte
        */
        public void GenerateCaveMap()
        {

            fillMap();


            for (int i = 0; i < smoothIterations; i++)
            {
                smoothMap();
            }

            // On récupère les rooms
            this.rooms = GetRooms();

            ProcessRooms();

        }


        /**
        * Renvoie n positions aléatoires disponibles sur la grille
        * @param n nombre de positions à renvoyer
        */
        public List<Vector2> GetRandomAvailablePositions(int n)
        {
            
            List<Vector2> positions = new List<Vector2>();

            for (int i = 0; i < n; i++)
            {
                positions.Add(GetRandomAvailablePosition());
            }

            return positions;

        }

        /**
        * Génère une position aléatoire disponible sur la grille
        */
        public Vector2 GetRandomAvailablePosition()
        {

            // On récupère une position dans la room 0
            Room room = this.rooms[0];

            Vector2 position = room.getRandomTile();

            // On enlève la taille de la grille pour que la position soit bien au milieu de la case
            position.x -= caveWidth / 2;
            position.y -= caveHeight / 2;

            return position;

        }

        /**
        * Traite les rooms
        */
        private void ProcessRooms()
        {

            int roomCount = this.rooms.Count;

            while (roomCount > 1)
            {

                ConnectRandomRoom();

                roomCount--;

            }

        }

        /**
        * Connecte la room 1 à sa room la plus proche
        */
        public void ConnectRandomRoom()
        {

            if (this.rooms.Count < 2)
            {
                Debug.LogError("Pas assez de rooms pour connecter");
                return;
            }

            // On récupère la première room
            Room room = this.rooms[0];

            // On récupère la room la plus proche
            Room closestRoom = room.GetClosestRoom(this.rooms);

            // On creuse un tunnel entre les deux rooms
            Room result = ConnectRooms(room, closestRoom);

            // On supprime les rooms de la liste
            this.rooms.Remove(room);
            this.rooms.Remove(closestRoom);

            // On ajoute la nouvelle room au début de la liste
            this.rooms.Insert(0, result);

        }


        /**
        * Remplit la map de grotte
        */
        private void fillMap()
        {

            // On crée la map
            caveMap = new int[caveWidth, caveHeight];

            // On la remplit
            for (int x = 0; x < caveWidth; x++)
            {

                for (int y = 0; y < caveHeight; y++)
                {

                    // Si on est sur les bords, on remplit la case
                    if (x == 0 || x == caveWidth - 1 || y == 0 || y == caveHeight - 1)
                    {
                        caveMap[x, y] = 1;
                        continue;
                    }

                    // On génère un nombre aléatoire
                    float random = Random.Range(0f, 1f);

                    // On remplit la case en fonction du taux de remplissage
                    caveMap[x, y] = random < fillRate ? 1 : 0;

                }

            }

        }

        /**
        * Affiche la map de grotte
        */
        public void DisplayCaveMap(Tilemap tilemap, string resourceLocation, Vector2 center)
        {
            // On parcourt la map
            for (int x = 0; x < caveWidth; x++)
            {

                for (int y = 0; y < caveHeight; y++)
                {
                    
                    // On récupère la valeur de la case
                    int value = caveMap[x, y];

                    if (value == 2)
                    {
                        continue;
                    }
                    
                    /****************************************************/
                    if(value == 1){ // Si la case est solide...
                        short neighbors = 0b000000; // Entier représentable comme un Mot binaire à 6 bits, représentant les voisins de la case (x, y) dans caveMap.
                        string spritePath = resourceLocation;


                        // Recherche des voisins dans caveMap.
                        if(y < caveHeight - 1 && caveMap[x, y+1] == 1){ // Haut
                            neighbors |= 0b100000;
                        }
                        if(y > 0){
                            if(caveMap[x, y-1] == 1){ // Bas
                                neighbors |= 0b010000;
                            }
                            if(x > 0 && caveMap[x-1, y-1] == 1){ // Diagonale Bas Gauche
                                neighbors |= 0b000010;
                            }
                            if(x < caveWidth - 1 && caveMap[x+1, y-1] == 1){ // Diagonale Bas Droite
                                neighbors |= 0b000001;
                            }
                        }
                        if(x > 0 && caveMap[x-1, y] == 1){ // Gauche
                            neighbors |= 0b001000;
                        }
                        if(x < caveWidth - 1 && caveMap[x+1, y] == 1){ // Droite
                            neighbors |= 0b000100;
                        }
                        
                        
                        // Gros switch faisant la correspondance entre mot binaire et sprite. 
                        switch(neighbors){
                            case 0b111111: case 0b111101: case 0b111110: case 0b111100:
                                spritePath += "all"; break;
                            case 0b101111:
                                spritePath += "all_but_down"; break;
                            case 0b101100:
                                spritePath += "all_but_down-both"; break;
                            case 0b101101:
                                spritePath += "all_but_down-down_left"; break;
                            case 0b101110:
                                spritePath += "all_but_down-down_right"; break;
                            case 0b110111: case 0b110101: case 0b110100: case 0b110110: case 0b100110:
                                spritePath += "all_but_left"; break;
                            case 0b111011: case 0b111010: case 0b111000: case 0b111001: case 0b101001:
                                spritePath += "all_but_right"; break;
                            case 0b011111: case 0b11101: case 0b11110:
                                spritePath += "all_but_up"; break;
                            case 0b000000: case 0b000001: case 0b000010: // Incertain vis à vis de ces deux dernièrs cases. (Bloc avec que des coins comme voisins)
                                spritePath += "alone"; break;
                            case 0b100000: case 0b100001: case 0b100010:
                                spritePath += "down"; break;
                            case 0b011010: case 0b011001: case 0b011011: case 0b011000:
                                spritePath += "down-left"; break;
                            case 0b010101: case 0b010110: case 0b010111: case 0b010100:
                                spritePath += "down_right"; break;
                            case 0b001000: case 0b001010: case 0b001011:
                                spritePath += "right"; break;
                            case 0b001001:
                                spritePath += "left-right"; break;
                            case 0b001100: case 0b011100:
                                spritePath += "left_right"; break;
                            case 0b001111:
                                spritePath += "left_right-both"; break;
                            case 0b001110:
                                spritePath += "left_right-left"; break;
                            case 0b001101:
                                spritePath += "left_right-right"; break;
                            case 0b000100: case 0b000101: case 0b000111:
                                spritePath += "left"; break;
                            case 0b000110:
                                spritePath += "right-left"; break;
                            case 0b010010: case 0b010011: case 0b010001: case 0b010000: 
                                spritePath += "up"; break;
                            case 0b110000: case 0b110001: case 0b110010: case 0b110011:
                                spritePath += "up_down"; break;
                            case 0b101000:
                                spritePath += "up_left"; break;
                            case 0b101010: case 0b101011:
                                spritePath += "up_left-left"; break;
                            case 0b100100:
                                spritePath += "up_right"; break;
                            case 0b100101: case 0b100111:
                                spritePath += "up_right-right"; break;
                        }

                        tilemap.SetTile(new Vector3Int(x + (int)center.x - caveWidth / 2, y + (int)center.y - caveHeight / 2, 0), Resources.Load<Tile>(spritePath));
                    }
                    /****************************************************/
                }
            }
        }

        /**
        * Lisse la map de grotte
        */
        private void smoothMap(int emptyValue = 0, int wallValue = 1)
        {

            for (int x = 0; x < caveWidth; x++)
            {

                for (int y = 0; y < caveHeight; y++)
                {

                    int wallCount = getWallCount(x, y);

                    if (wallCount > 4)
                    {
                        caveMap[x, y] = wallValue;
                    }
                    else if (wallCount < 4)
                    {
                        caveMap[x, y] = emptyValue;
                    }

                }

            }

        }

        /**
        * Renvoie le nombre de voisins d'une case
        */
        private int getWallCount(int x, int y)
        {

            int wallCount = 0;

            for (int i = x - 1; i <= x + 1; i++)
            {

                for (int j = y - 1; j <= y + 1; j++)
                {

                    if (i == x && j == y)
                    {
                        continue;
                    }

                    if (i < 0 || i >= caveWidth || j < 0 || j >= caveHeight)
                    {
                        wallCount++;
                        continue;
                    }

                    wallCount += caveMap[i, j];

                }

            }

            return wallCount;

        }

        /**
        * On recherche toutes les salles de la grotte
        */
        public List<Room> GetRooms()
        {

            // On crée une liste de salles
            List<Room> rooms = new List<Room>();

            // On recherche les 0 dans la map, on regarde leurs voisins, etc. pour en faire des salles
            // On remplace 0 par 2 pour ne pas les revisiter
            for (int x = 0; x < caveWidth; x++)
            {

                for (int y = 0; y < caveHeight; y++)
                {

                    // Si on trouve un 0, on crée une salle
                    if (caveMap[x, y] == 0)
                    {

                        // On récupère les coordonnées de la salle
                        List<Vector2Int> roomCoordinates = new List<Vector2Int>();
                        GetRoomCoordinates(new Vector2Int(x, y), roomCoordinates);

                        // On crée la salle
                        Room room = new Room(roomCoordinates, caveMap);

                        if (room.roomSize < caveThreshold)
                        {
                            continue;
                        }

                        // On remplace les 0 par des 2
                        foreach (Vector2Int coordinates in roomCoordinates)
                        {
                            caveMap[coordinates.x, coordinates.y] = 2;
                        }

                        // On ajoute la salle à la liste
                        rooms.Add(room);

                    }

                }

            }

            return rooms;

        }

        /**
        * Connecte deux salles en créant un tunnel
        */
        private Room ConnectRooms(Room roomA, Room roomB)
        {

            // On récupère les points les plus proches
            Vector2Int[] corners = roomA.GetClosestPoints(roomB);
            Vector2Int pointA = corners[0];
            Vector2Int pointB = corners[1];

            Coordinates coordinatesA = new Coordinates(pointA.x, pointA.y);
            Coordinates coordinatesB = new Coordinates(pointB.x, pointB.y);

            Line line = new Line(coordinatesA, coordinatesB);
            List<Coordinates> points = new List<Coordinates>();

            // On récupère les points du tunnel
            points = line.GetCoordinates();

            List<Vector2Int> pointsToRemove = new List<Vector2Int>();

            int currentTunnelSize = tunnelSize + Random.Range(-1, tunnelVariation);

            foreach (Coordinates coordinates in points)
            {

                // On trace un cercle autour du point


                Circle(currentTunnelSize, coordinates.x, coordinates.y, 2, pointsToRemove);

            }


            // On crée le tunnel
            Room tunnel = new Room(pointsToRemove, caveMap);

            // On fusionne les deux salles
            Room room = roomA.MergeRooms(roomB, rooms);

            // On fusionne le tunnel avec la salle
            Room result = room.MergeRooms(tunnel, rooms);

            return result;

        }

        /**
        * On dessine un cercle
        */
        private void Circle(int radius, int x, int y, int value, List<Vector2Int> pointsToRemove)
        {

            // On récupère le rayon au carré
            int radiusSquared = radius * radius;

            // On parcourt le cercle
            for (int i = -radius; i <= radius; i++)
            {

                for (int j = -radius; j <= radius; j++)
                {

                    // On vérifie que l'on est bien dans le cercle
                    if (i * i + j * j <= radiusSquared)
                    {

                        // On récupère les coordonnées
                        int xCoord = x + i;
                        int yCoord = y + j;

                        // On vérifie que l'on est bien dans la map
                        if (xCoord >= 1 && xCoord < caveWidth - 1 && yCoord >= 1 && yCoord < caveHeight - 1)
                        {

                            // On remplit la case
                            caveMap[xCoord, yCoord] = value;

                            // On ajoute les coordonnées à la liste
                            pointsToRemove.Add(new Vector2Int(xCoord, yCoord));

                        }

                    }

                }

            }


        }


        /**
        * Renvoie les coordonnées de la salle actuelle
        * On le fait récursivement : on part d'une case, on regarde ses voisins, on regarde les voisins de ses voisins, etc.
        * On note les cases visitées pour ne pas les revisiter
        */
        private void GetRoomCoordinates(Vector2Int coordinates, List<Vector2Int> roomCoordinates, HashSet<Vector2Int> visited = null)
        {

            // On note la case comme visitée
            if (visited == null)
            {
                visited = new HashSet<Vector2Int>();
            }

            visited.Add(coordinates);

            // On ajoute les coordonnées à la liste
            roomCoordinates.Add(coordinates);

            // On récupère les voisins
            List<Vector2Int> neighbors = GetNeighbors(coordinates);

            // On parcourt les voisins
            foreach (Vector2Int neighbor in neighbors)
            {

                // Si le voisin est déjà visité, on passe
                if (caveMap[neighbor.x, neighbor.y] == 1 || visited.Contains(neighbor))
                {
                    continue;
                }

                GetRoomCoordinates(neighbor, roomCoordinates, visited);
            }

        }

        /**
        * Renvoie les voisins d'une case
        */
        private List<Vector2Int> GetNeighbors(Vector2Int coordinates)
        {

            List<Vector2Int> neighbors = new List<Vector2Int>();

            // On parcourt les directions
            foreach (Vector2Int direction in directions)
            {

                // On récupère les coordonnées du voisin
                Vector2Int neighbor = coordinates + direction;

                // On vérifie que le voisin est dans la map
                if (neighbor.x >= 0 && neighbor.x < caveWidth && neighbor.y >= 0 && neighbor.y < caveHeight)
                {
                    neighbors.Add(neighbor);
                }

            }

            return neighbors;

        }


        public class Coordinates
        {

            public int x;
            public int y;

            public Coordinates(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

            public override int GetHashCode()
            {
                return x.GetHashCode() ^ y.GetHashCode();
            }

            public override bool Equals(object obj)
            {

                if (!(obj is Coordinates))
                {
                    return false;
                }

                Coordinates other = (Coordinates)obj;

                return x == other.x && y == other.y;

            }

        }

        public class Line
        {

            // Une ligne est un ensemble de coordonnées
            private List<Coordinates> coordinates;
            private Coordinates point1;
            private Coordinates point2;

            public Line(Coordinates point1, Coordinates point2)
            {

                this.point1 = point1;
                this.point2 = point2;

            }

            public List<Coordinates> GetCoordinates()
            {

                if (coordinates == null)
                {
                    coordinates = new List<Coordinates>();
                    ComputeCoordinatesCurve();
                }

                return coordinates;

            }

            private void ComputeCoordinatesLine()
            {

                // On doit relier les deux points en traçant une ligne

                // On récupère les coordonnées des points
                int x1 = point1.x;
                int y1 = point1.y;
                int x2 = point2.x;
                int y2 = point2.y;

                // On calcule le gradient
                int gradientX = x2 - x1;
                int gradientY = y2 - y1;

                // On calcule le nombre de points à tracer
                int pointsCount = Mathf.Max(Mathf.Abs(gradientX), Mathf.Abs(gradientY));

                // On trace les points
                for (int i = 0; i <= pointsCount; i++)
                {

                    // On calcule les coordonnées du point
                    int x = x1 + (int)Mathf.Round((float)i / pointsCount * gradientX);
                    int y = y1 + (int)Mathf.Round((float)i / pointsCount * gradientY);

                    // On ajoute le point à la liste
                    coordinates.Add(new Coordinates(x, y));

                }

            }

            private void ComputeCoordinatesCurve()
            {

                // On doit relier les deux points en traçant une courbe quadratique
                int x1 = point1.x;
                int y1 = point1.y;
                int x2 = point2.x;
                int y2 = point2.y;

                int x;
                int y;

                if (Mathf.Abs(x1 - x2) < Mathf.Abs(y1 - y2)) {

                    x = (x1 + x2) / 2;
                    y = (y1 + y2) / 2 + Random.Range(-1, 2) * (int)Mathf.Round(Mathf.Abs(x2 - x1) / 2f);                

                } else {

                    x = (x1 + x2) / 2 + Random.Range(-1, 2) * (int)Mathf.Round(Mathf.Abs(y2 - y1) / 2f);
                    y = (y1 + y2) / 2;

                }


                Coordinates middle = new Coordinates(x, y);

                // On fait un lerp entre les deux points pour avoir une courbe
                for (float i = 0; i <= 1; i += 0.01f)
                {

                    int xLerp = (int)Mathf.Round(Mathf.Lerp(x1, x2, i));
                    int yLerp = (int)Mathf.Round(Mathf.Lerp(y1, y2, i));

                    int xMiddle = (int)Mathf.Round(Mathf.Lerp(x1, x2, i + 0.1f));
                    int yMiddle = (int)Mathf.Round(Mathf.Lerp(y1, y2, i + 0.1f));

                    int xCurve = (int)Mathf.Round(Mathf.Lerp(xLerp, xMiddle, i));
                    int yCurve = (int)Mathf.Round(Mathf.Lerp(yLerp, yMiddle, i));

                    coordinates.Add(new Coordinates(xCurve, yCurve));

                }

            }

        }

        public class CoordPair
        {

            public Vector2Int coordA;
            public Vector2Int coordB;

            public CoordPair(Vector2Int coordA, Vector2Int coordB)
            {
                this.coordA = coordA;
                this.coordB = coordB;
            }

            public override int GetHashCode()
            {
                return coordA.GetHashCode() ^ coordB.GetHashCode();
            }

            public override bool Equals(object obj)
            {

                if (!(obj is CoordPair))
                {
                    return false;
                }

                CoordPair other = (CoordPair)obj;

                return (coordA == other.coordA && coordB == other.coordB) || (coordA == other.coordB && coordB == other.coordA);

            }

        }

        public class Room
        {

            public List<Vector2Int> tiles;
            public List<Vector2Int> edgeTiles;
            public int roomSize;
            public int[,] caveMap;
            private int caveWidth = 0;
            private int caveHeight = 0;

            // On va stocker les distances entre les salles pour ne pas avoir à les recalculer à chaque fois
            private Dictionary<Room, CoordPair> closestPoints = new Dictionary<Room, CoordPair>();

            private List<Vector2Int> directions = new List<Vector2Int>()
            {
                new Vector2Int(0, 1),
                new Vector2Int(0, -1),
                new Vector2Int(1, 0),
                new Vector2Int(-1, 0),
            };

            public Room(int[,] map)
            {

                tiles = new List<Vector2Int>();
                edgeTiles = new List<Vector2Int>();
                roomSize = 0;
                caveMap = map;
                caveWidth = map.GetLength(0);
                caveHeight = map.GetLength(1);

                closestPoints = new Dictionary<Room, CoordPair>();

            }

            public Room(List<Vector2Int> roomTiles, int[,] map)
            {

                tiles = roomTiles;
                roomSize = tiles.Count;
                caveMap = map;
                caveWidth = map.GetLength(0);
                caveHeight = map.GetLength(1);

                edgeTiles = getEdgeTiles();

                closestPoints = new Dictionary<Room, CoordPair>();

            }

            /**
            * Renvoie une position aléatoire dans la salle
            */
            public Vector2Int getRandomTile()
            {
                return tiles[Random.Range(0, tiles.Count)];
            }


            /**
            * Renvoie les coins de la salle
            */
            public List<Vector2Int> getEdgeTiles()
            {

                List<Vector2Int> corners = new List<Vector2Int>();

                foreach (Vector2Int tile in tiles)
                {

                    foreach (Vector2Int direction in directions)
                    {

                        Vector2Int neighbor = tile + direction;

                        if (neighbor.x < 0 || neighbor.x >= caveWidth || neighbor.y < 0 || neighbor.y >= caveHeight)
                        {
                            continue;
                        }

                        if (caveMap[neighbor.x, neighbor.y] == 1 && !corners.Contains(tile))
                        {
                            corners.Add(tile);
                        }

                    }

                }

                return corners;

            }

            /** 
            * Fusionne deux salles
            */
            public Room MergeRooms(Room room, List<Room> rooms)
            {

                this.tiles.AddRange(room.tiles);
                this.edgeTiles.AddRange(room.edgeTiles);
                this.roomSize += room.roomSize;

                // On enlève toutes les données de rooms en rapport avec la salle fusionnée 
                foreach (Room otherRoom in rooms)
                {

                    if (otherRoom == room || otherRoom == this)
                    {
                        continue;
                    }

                    if (otherRoom.closestPoints.ContainsKey(room))
                    {
                        otherRoom.closestPoints.Remove(room);
                    }

                    if (otherRoom.closestPoints.ContainsKey(this))
                    {
                        otherRoom.closestPoints.Remove(this);
                    }

                }

                this.closestPoints.Clear();

                return this;
            }

            /**
            * Renvoie les deux points les plus proches entre deux salles
            */
            public Vector2Int[] GetClosestPoints(Room room)
            {

                if (this.closestPoints.ContainsKey(room))
                {

                    Vector2Int[] converted = new Vector2Int[2];
                    converted[0] = this.closestPoints[room].coordA;
                    converted[1] = this.closestPoints[room].coordB;

                    return converted;

                }

                Vector2Int[] closestPointsBetweenRooms = new Vector2Int[2];
                int closestDistance = int.MaxValue;

                foreach (Vector2Int tileA in edgeTiles)
                {

                    foreach (Vector2Int tileB in room.edgeTiles)
                    {

                        int distance = Mathf.Abs(tileA.x - tileB.x) + Mathf.Abs(tileA.y - tileB.y);

                        if (distance < closestDistance)
                        {
                            closestDistance = distance;
                            closestPointsBetweenRooms[0] = tileA;
                            closestPointsBetweenRooms[1] = tileB;
                        }

                    }

                }

                CoordPair coordPair = new CoordPair(closestPointsBetweenRooms[0], closestPointsBetweenRooms[1]);
                this.closestPoints.Add(room, coordPair);
                room.closestPoints.Add(this, coordPair);

                return closestPointsBetweenRooms;

            }

            /**
            * Renvoie la distance entre deux salles
            */
            public float GetDistance(Room room)
            {

                Vector2Int[] closestPoints = GetClosestPoints(room);

                int distance = Mathf.Abs(closestPoints[0].x - closestPoints[1].x) + Mathf.Abs(closestPoints[0].y - closestPoints[1].y);

                return distance;

            }

            /**
            * Renvoie la salle la plus proche
            */
            public Room GetClosestRoom(List<Room> rooms)
            {

                Room closestRoom = null;
                int closestDistance = int.MaxValue;

                foreach (Room room in rooms)
                {

                    if (room == this)
                    {
                        continue;
                    }

                    int distance = (int)GetDistance(room);

                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestRoom = room;
                    }

                }

                return closestRoom;


            }


        }

    }
}