using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

using Dream.Collectible;

namespace Dream.MapManagement {
    public class MapManager : MonoBehaviour {

        // Définition de la cave
        [SerializeField]
        private Cave cave;

        [SerializeField]
        private string tileMapResourcesFolderLocation;

        // Le fond à utiliser (lecture uniquement)
        [SerializeField]
        private Sprite _backgroundSprite;
        public Sprite BackgroundSprite {
            get => _backgroundSprite;
        }

        public Vector2 Size {
            get => new Vector2(cave.caveWidth, cave.caveHeight);
        }

        private Tilemap tilemap;

        [SerializeField] private Coin coinPrefab;
        [SerializeField, Range(0, 100)] private int coinNumber = 10;

        List<Coin> coins = new List<Coin>();

        void Awake()
        {
            tilemap = GetComponentInChildren<Tilemap>();

            // On génère une map et on place les coins
            generateMapAndCoins();

        }

        private void DeleteExistingCoins() {
            // Si on a déjà des coins, on les supprime
            if (coins.Count > 0)
            {
                foreach (Coin coin in coins)
                {
                    Destroy(coin.gameObject);
                }
                coins.Clear();
            }
        }

        /**
        * Génère une map et l'affiche (avec les coins)
        */
        private void generateMapAndCoins()
        {
            DeleteExistingCoins();

            cave.GenerateCaveMap();
            cave.DisplayCaveMap(tilemap, tileMapResourcesFolderLocation, Vector2.zero);

            List<Vector2> possiblePositions = cave.GetRandomAvailablePositions(coinNumber);

            // On place les coins
            for (int i = 0; i < coinNumber; i++)
            {
                // On récupère une position aléatoire
                Vector2 randomPosition = possiblePositions[i];

                // On rajoute une demi-taille de tile pour être au centre de la tile
                randomPosition += new Vector2(0.5f, 0.5f);

                // On place un coin
                Coin coin = Instantiate(coinPrefab, randomPosition, Quaternion.identity);
                coins.Add(coin);

            }

        }

        public Vector2 GetRandomAvailablePosition() {
            return cave.GetRandomAvailablePosition();
        }

    }
}
