using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Dream.PlayerLogic;

namespace Dream.MapManagement {

    public class RandomDream : MonoBehaviour {

        [SerializeField]
        private MapManager[] mapManagers;

        [SerializeField]
        private SpriteRenderer background;

        [SerializeField]
        private Player player;

        private static MapManager current = null;

        void Awake() {

            // Il ne doit y avoir qu'un seul rêve en cours
            if (current != null) {
                Destroy(current);
            }

            // On génère un des rêves possibles aléatoirement
            current = Instantiate(mapManagers[Random.Range(0, mapManagers.Length)]); 
        }

        void Start() {
            PlayerPosition();
            InitializeBackground();
        }

        void InitializeBackground() {
            background.sprite = current.BackgroundSprite;
            background.size = current.Size;
        }

        void PlayerPosition() {
            player.transform.position = current.GetRandomAvailablePosition();
        }
    }
}