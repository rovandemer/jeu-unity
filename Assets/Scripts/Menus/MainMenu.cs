using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Shared.Save.Writers;

namespace Menus {
    public class MainMenu : MonoBehaviour {

        public void StartGame() {
            SceneManager.LoadScene("TowerDefenseSection");
        }

        public void Reset() {

            PlayerSave.Delete();
            BuildingSave.Delete();

            StartGame();
        }

        public void Options() {
            // TODO
        }

        public void QuitGame() {
            Application.Quit();
            Debug.Log("Quit");
        }

    }
}
