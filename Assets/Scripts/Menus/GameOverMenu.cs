using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Shared.Save.Writers;

namespace Menus {
    public class GameOverMenu : MonoBehaviour {

        public void RestartGame() {
            // En cas d'échec, on retroune à la section de TowerDefense
            SceneManager.LoadScene("TowerDefenseSection");
        }

        public void Options() {
            // TODO
        }

        public void Reset() {

            PlayerSave.Delete();
            BuildingSave.Delete();

            RestartGame();
        }

        public void QuitGame() {
            SceneManager.LoadScene("MainMenu");
        }

    }
}