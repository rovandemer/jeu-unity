using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

using Shared.Input;

namespace Menus {
    public class PauseMenu : MonoBehaviour {

        bool isPaused = false;
        [SerializeField]
        GameObject pauseMenu;
        GameObject[] userInterfaces;

        PlayerInputActions input;

        void OnEnable() {
            input = GameInput.inputActions;
            input.UI.Pause.Enable();
            input.UI.Pause.performed += PauseUnpause;
        }

        void OnDisable() {
            input.UI.Pause.performed -= PauseUnpause;
            input.UI.Pause.Disable();
            input = null;
        }

        void Start() {
            // Certains objets récupérés par cette méthode seront détruit à l'appel au premier Update
            // cf. https://docs.unity3d.com/ScriptReference/Object.Destroy.html
            // On devra vérifier qu'ils n'ont pas été détruit en cas d'appel de méthode sur un objet de userInterfaces
            userInterfaces = GameObject.FindGameObjectsWithTag("UI");
        }

        void PauseUnpause(InputAction.CallbackContext _) {

            
            if (isPaused) {
                ResumeGame();
            }
            else {
                PauseGame();
            }
        }



        /**
        *  Relance le jeu. Peut être appelé par l'appui sur l'UI ou alors en appuyant sur la touche échap une fois le menu ouvert.
        */
        public void ResumeGame() {

            SetUIActive(true);

            isPaused = false;
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
        }

        /**
        *  Met en pause le jeu. Est appelé à l'appui de la touche échap si le menu n'est pas ouvert.
        */
        void PauseGame() {

            // On ne veut pas que l'on puisse placer des bâtiments pendant l'ouverture du menu pause.
            SetUIActive(false);

            isPaused = true;
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
        }

        public void QuitGame() {
            ResumeGame();
            SceneManager.LoadScene("MainMenu");
            DestroyUI();
        }

        /**
        *  Pas encore implémenté.
        */
        public void Options() {
            Debug.Log("Options");
        }

        /**
        *  Active ou désactive tous les objets marqués du tag "UI".
        */
        void SetUIActive(bool isActive) {

            foreach (GameObject ui in userInterfaces) {
                if (ui != null) {
                    ui.SetActive(isActive);
                }
            }
        }

        void DestroyUI() {
            foreach (GameObject ui in userInterfaces) {
                if (ui != null) {
                    Destroy(ui);
                }
            }
        }
        
    }
}
