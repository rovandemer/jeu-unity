using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/**
 *  Classe commune au monde de rêve et au monde réel. Elle gère l'argent du joueur
 */
namespace Shared.Indicators {

    public class MoneyCounter : MonoBehaviour {

        public static MoneyCounter instance = null;

        [SerializeField]
        private TextMeshProUGUI moneyCount;

        void Awake() {
            // On veut que cet objet ou un de ses parents ait un KeepLoaded
            if (GetComponentInParent<KeepLoaded>() == null) {
                Debug.LogError($" {this.gameObject} or its parent needs a KeepLoaded component");
            }

            if (instance == null) {
                instance = this;
            }
        }

        public void UpdateMoneyCount(int value) {
            if (value < 0) {
                Debug.LogError($"Negative money value invalid : {value}");
            }
            moneyCount.text = FormatMoneyValue(value);
        }

        /**
        *  Formatte la quantité d'argent pour ne pas dépasser le compteur
        */
        private static string FormatMoneyValue(int value) {
            return (value < 1E7) ? value.ToString() : "9999999+";
        }

    }
}
