using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Indicators {
    public class KeepLoaded : MonoBehaviour {

        private static GameObject keepLoaded = null;

        void Awake() {

            if (keepLoaded != null) {
                Destroy(this.gameObject);
                return;
            }

            DontDestroyOnLoad(this.gameObject);
            keepLoaded = this.gameObject;
        }

        public void Destroy() {
            Destroy(this.gameObject);
        }
    }
}
