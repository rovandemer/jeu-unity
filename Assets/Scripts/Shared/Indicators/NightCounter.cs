using UnityEngine;
using TMPro;

/**
 *  Classe permettant de maintenir la référence au nightCounter courant
 *  c'est-à-dire la référence au compteur de nuit indiquant au joueur la nuit courante.
 */
namespace Shared.Indicators {

    public class NightCounter : MonoBehaviour {

        [SerializeField]
        private TextMeshProUGUI nightCounter;

        public static NightCounter instance;

        void Awake() {

            // On veut que cet objet ou un de ses parents ait un KeepLoaded
            if (GetComponentInParent<KeepLoaded>() == null) {
                Debug.LogError($" {this.gameObject} or its parent needs a KeepLoaded component");
            }

            if (instance == null) {
                instance = this;
            }
        }

        public void UpdateNightCount(int value) {
            if (value < 0) {
                Debug.LogError($"Negative nightNumber invalid : {value}");
            }
            nightCounter.text = value.ToString();
        }

    }
}
