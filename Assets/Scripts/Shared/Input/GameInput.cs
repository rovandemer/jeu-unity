using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

/**
 *  Classe servant à avoir un input généralisé entre toutes les classes 
 *  (On peut vouloir désactiver des fonctionnalités lorsqu'on est dans un menu par exemple).
 */
namespace Shared.Input {
    public static class GameInput {

        public static PlayerInputActions inputActions {get; private set;}

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void InitializeInput() {
            inputActions = new PlayerInputActions();

            Application.quitting += () => {
                inputActions.Dispose();
            };
        }

    }
}