using System;
using UnityEngine;
using System.Collections;

/**
 *  Coroutines permettant de lancer des actions ou d'autres coroutines avec des paramètres temporels
 *  Elle permettent d'executer una Action à chaque frame ou lancer une action ou une coroutine au bout d'un certain temps
 */

namespace Shared.Utils {
    public static class Time {

        /**
        *  Lance une action au bout du délai spécifié.
        *  @param realTime Utiliser le temps réel ou le temps de Time.time
        */
        public static IEnumerator WithDelay(System.Action action, float delay, bool realTime = false) {
            yield return (realTime) ? new WaitForSecondsRealtime(delay) : new WaitForSeconds(delay);
            action();
        }

        /**
        *  Fait exécuter la coroutine au bout du délai spécifié
        *  @param executer Le MonoBehaviour qui exécutera la requête
        *  @param realTime Utiliser le temps réel ou le temps de Time.time
        */
        public static IEnumerator WithDelay(MonoBehaviour executer, IEnumerator coroutine, float delay, bool realTime = false) {
            yield return (realTime) ? new WaitForSecondsRealtime(delay) : new WaitForSeconds(delay);
            executer.StartCoroutine(coroutine);
        }

        public static IEnumerator ExecuteEachFrame(System.Action action, float duration, bool realTime = false) {

            float endOfExectutionTime;

            // NOTE : Probablement pas la manière la plus élégante de faire
            // mais je ne vois pas de méthode qui n'impliquerait pas des appels de méthodes renvoyant un booléen composé d'une comparaison 
            if (!realTime) {
                endOfExectutionTime = UnityEngine.Time.time + duration;

                while (UnityEngine.Time.time < endOfExectutionTime) {
                    action();
                    yield return false;
                }
            }
            else {
                endOfExectutionTime = UnityEngine.Time.realtimeSinceStartup + duration;

                while (UnityEngine.Time.realtimeSinceStartup < endOfExectutionTime) {
                    action();
                    yield return false;
                }
            }
        }

    }
}
