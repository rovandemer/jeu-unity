using UnityEngine;

namespace Shared.Utils.GridExtensionMethods {

    public static class GridExtensions {

        public static Vector3 CellCenter(this Grid grid, Vector3 worldPosition) {
            return grid.GetCellCenterWorld(grid.WorldToCell(worldPosition));
        }

        public static int WorldToHorizontalCellPosition(this Grid grid, Vector3 position) {
            return grid.WorldToCell(position).x;
        }

        public static Vector3 CellToWorldCenter(this Grid grid, int x, int y) {
            Vector3Int cellPosition = new Vector3Int(x, y, 0);
            return grid.GetCellCenterWorld(cellPosition);
        }

    }

}
