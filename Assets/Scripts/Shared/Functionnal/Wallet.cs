using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using TowerDefense.Buildings;
using TowerDefense.Buildings.Management;

namespace Shared.Functionnal {
    public class Wallet : MonoBehaviour {

        [SerializeField]
        private int InitialMoney;

        private int _money;
        public int Money {
            get => _money;
            private set {
                _money = value;
                // On veut mettre le compteur à jour à chaque set, 
                Shared.Indicators.
                            MoneyCounter.instance.UpdateMoneyCount(value);
            }
        }

        public static Wallet instance {get; private set;} = null;

        void Awake() {
            if (instance == null) {
                instance = this;
                SceneManager.sceneLoaded += SubscribeToCurrentBuildingManager;
            }
        }

        void SubscribeToCurrentBuildingManager(Scene scene, LoadSceneMode mode) {
            if (BuildingManager.instance != null) {
                BuildingManager.instance.BuildingPlaced += BuyBuilding;
            }
        }

        /**
        *  Initialise le champ d'argent à l'aide d'éventuelles données de sauvegarde
        */
        public void InitMoney(Shared.Save.Data.PlayerData data) {
            this.Money = (data != null) ? data.money : InitialMoney;
        }

        /**
        *  Récupère une pièce du monde de rêve.
        */
        public void GetCoin(Dream.Collectible.Coin coin) {
            this.Money += coin.GetCoin();
        }


        /**
        *  Renvoie si l'utilisateur peut dépenser l'argent passé en argument.
        */
        public bool CanSpendMoney(int value) {
            return value <= this.Money;
        }

        public void BuyBuilding(Building building) {
            if (building == null) {
                return;
            }
            SpendMoney(building.Cost);
        }

        /**
        *  Dépense l'argent indiqué.
        */
        public void SpendMoney(int value) {
            this.Money -= value;
        }
    }
}