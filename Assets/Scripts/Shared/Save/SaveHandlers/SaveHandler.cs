using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Shared.Functionnal;
using TowerDefense.Buildings.Management;
using TowerDefense.DayNight;

/**
 *  Permet d'enregistrer les données du jeu à deux moments : Avant de commencer la section de TowerDefense et avant de lancer le monde de rêve
 */

namespace Shared.Save {

    public static class SaveHandler {

        public static void SaveGameDay() {
            // Le jour, on doit enregistrer les bâtiments (qui seront sinon perdus au chargement de la scène de rêve)
            BuildingManager.instance.SaveBuildings();
            Debug.Log($"PlayerSave.Save(Night: {DayNightManager.NightNumber}, Money: {Wallet.instance.Money}, isNight: false)");
            Writers.PlayerSave.Save(DayNightManager.NightNumber, Wallet.instance.Money, false);
        }

        public static void SaveGameNight() {

            Debug.Log($"PlayerSave.Save(Night: {DayNightManager.NightNumber}, Money: {Wallet.instance.Money}, isNight: true)");
            Writers.PlayerSave.Save(DayNightManager.NightNumber, Wallet.instance.Money, true);
        }
    }
}