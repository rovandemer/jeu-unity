using UnityEngine;
using UnityEngine.SceneManagement;

using Shared.Functionnal;
using TowerDefense.Buildings.Management;
using TowerDefense.DayNight;

namespace Shared.Save {
    public class SaveLoader : MonoBehaviour {

        private void LoadGame() {

            Data.PlayerData data = Writers.PlayerSave.Load();
            // data peut être null

            DayNightManager.instance.LoadSaveData(data);
            Wallet.instance.InitMoney(data);
        }

        void Start() {
            LoadGame();
            BuildingManager.instance.LoadBuildings();
        }
    }
}
