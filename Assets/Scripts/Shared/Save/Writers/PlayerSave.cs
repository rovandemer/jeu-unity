using System;
using System.Text;
using UnityEngine;
using System.IO;

using Shared.Save.Data;

namespace Shared.Save.Writers {
    public static class PlayerSave {

        public static string path = Path.Combine(Application.persistentDataPath, "player.save");

        public static void Save(int nightNumber, int money, bool isNight) {

            PlayerData data = new PlayerData(nightNumber, money, isNight);
            if (!data.IsValid()) {
                return;
            }
            string playerData = JsonUtility.ToJson(data);

            using (FileStream file = new FileStream(path, FileMode.Create)) {
                using (BinaryWriter writer = new BinaryWriter(file, Encoding.UTF8)) {
                    writer.Write(playerData);
                }
            }
        }

        public static PlayerData Load() {

            if (!File.Exists(path)) {
                Debug.Log($"Not found : {path}");
                return null;
            }

            string jsonPlayerData;
            using (FileStream file = new FileStream(path, FileMode.Open)) {
                using (BinaryReader reader = new BinaryReader(file, Encoding.UTF8)) {
                    jsonPlayerData = reader.ReadString();
                }
            }

            PlayerData data;
            try {
                data = JsonUtility.FromJson<PlayerData>(jsonPlayerData);
            }
            catch (ArgumentException) {
                Debug.LogWarning("Corrupt data at : {path}, resetting game.");
                PlayerSave.Delete();
                BuildingSave.Delete();
                return null;
            }
            return (data.IsDefault()) ? null : data;

        }

        public static bool Delete() {

            if (!File.Exists(path)) {
                return false;
            }
            File.Delete(path);
            return true;
        }

    }
}

