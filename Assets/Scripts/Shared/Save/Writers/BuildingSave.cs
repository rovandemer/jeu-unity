using System;
using System.Text;
using UnityEngine;
using System.IO;

using TowerDefense.Buildings;

namespace Shared.Save.Writers {
    public static class BuildingSave {

        public static string path = Path.Combine(Application.persistentDataPath, "building.save");

        public static void Save(Building[] buildings) {

            BuildingData data = new BuildingData(buildings);

            string jsonData = JsonUtility.ToJson(data);
            using (FileStream file = new FileStream(path, FileMode.Create)) {
                using (BinaryWriter writer = new BinaryWriter(file, Encoding.UTF8)) {
                    writer.Write(jsonData);
                }
            }
        }

        public static BuildingData Load() {

            if (!File.Exists(path)) {
                Debug.Log($"Not found : {path}");
                // Empty BuildingData
                return new BuildingData();
            }

            string jsonBuildingData;
            using (FileStream file = new FileStream(path, FileMode.Open)) {
                using (BinaryReader reader = new BinaryReader(file, Encoding.UTF8)) {
                    jsonBuildingData = reader.ReadString();
                }
            }

            try {
                return JsonUtility.FromJson<BuildingData>(jsonBuildingData);
            }
            catch (ArgumentException) {
                Debug.LogWarning("Corrupt data at : {path}, resetting building data.");
                PlayerSave.Delete();
                BuildingSave.Delete();
                return new BuildingData();
            }
        }

        public static bool Delete() {

            if (!File.Exists(path)) {
                return false;
            }
            File.Delete(path);
            return true;
        }

    }
}
