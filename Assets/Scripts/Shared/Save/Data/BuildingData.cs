using UnityEngine;
using System;

using TowerDefense.Buildings;
using TowerDefense.Buildings.Management;

[System.Serializable]
public class BuildingData {

    public int[] buildingPositions;
    public int[] buildingNumbers;
    public int[] buildingHealths;

    public BuildingData() {
        buildingPositions = Array.Empty<int>();
        buildingNumbers = Array.Empty<int>();
    }

    public BuildingData(Building[] buildings) {

        int index = 0;

        buildingPositions = new int[buildings.Length];
        buildingNumbers = new int[buildings.Length];
        buildingHealths = new int[buildings.Length];

        foreach (Building building in buildings) {
            buildingPositions[index] = BuildingManager.instance.buildingGrid.LocalToCell(building.transform.position).x;
            buildingNumbers[index] = BuildingManager.instance.GetBuildingNumber(building.name);
            buildingHealths[index] = building.Health;
            index++;
        }

    }

}
