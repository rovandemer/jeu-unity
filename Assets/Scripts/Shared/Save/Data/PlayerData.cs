using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shared.Save.Data {
    [System.Serializable]
    public class PlayerData {

        public int nightNumber;
        public int money;
        public bool isNight;

        public PlayerData(int nightNumber, int money, bool isNight) {

            this.nightNumber = nightNumber;
            this.money = money;
            this.isNight = isNight;

            if (!this.IsValid()) {
                Debug.LogError($"Invalid PlayerData : (nightNumber: {nightNumber}, money: {money})");
            }
        }

        public bool IsDefault() {
            return nightNumber == default(int) && money == default(int) && isNight == default(bool);
        }

        public bool IsValid() {
            return nightNumber >= 0 && money >= 0;
        }

        override public string ToString() {
            return $"(nightNumber : {this.nightNumber}, money : {this.money}, isNight : {this.isNight})";
        }

    }
}
